#ifndef _ALGO_H_
#define _ALGO_H_

#include "metabase.h"

#include <string>
#include <numeric>
#include <string_view>
#include <type_traits>

namespace util {

template <class InputIt>
std::enable_if_t<mtl::is_input_iterator_v<InputIt>,
    std::string> join(InputIt first, InputIt last, std::string_view sep) {
    if (first == last)
        return {};
    return std::accumulate(std::next(first), last, *first, [sep](const auto& a, const auto& b) {
        return a + std::string(sep) + b;
    });
}

template <class InputIt, class UnaryOp>
std::enable_if_t<mtl::is_input_iterator_v<InputIt>,
    std::string> join(InputIt first, InputIt last, std::string_view sep, UnaryOp f) {
    if (first == last)
        return {};
    return std::accumulate(std::next(first), last, f(*first), [sep, f](const auto& a, const auto& b) {
        return a + std::string(sep) + f(b);
    });
}

} // util

#endif // _ALGO_H_
