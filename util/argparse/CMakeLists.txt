set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(argparse)

set(SOURCE_LIB
    argparse.cpp argparse.h
)

add_library(argparse STATIC ${SOURCE_LIB})

add_subdirectory(ut)
