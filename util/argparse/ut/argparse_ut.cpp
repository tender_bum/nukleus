#include <iostream>
#include <string>
#include <regex>

int main(int argc, const char* argv[]) {
    std::regex long_arg_re("--([[:w:]](\\w|-)+)");
    std::regex small_arg_re("-([[:w:]])");
    std::match_results<const char*> base_match;

    std::cout << "Long arg filter:" << std::endl;
    for (int i = 1; i < argc; ++i) {
        std::cout << argv[i] << ":" << std::endl;
        if (std::regex_match(argv[i], base_match, long_arg_re)) {
            std::cout << "\tmatch: " << base_match.str() << std::endl;
            for (int j = 0; j < (int)base_match.size(); ++j)
                std::cout << "\t\tsubmatch " << j << ": " << base_match[j].str() << std::endl;
        }
    }
    std::cout << "Small arg filter:" << std::endl;
    for (int i = 1; i < argc; ++i) {
        std::cout << argv[i] << ":" << std::endl;
        if (std::regex_match(argv[i], base_match, small_arg_re)) {
            std::cout << "\tmatch: " << base_match.str() << std::endl;
            for (int j = 0; j < (int)base_match.size(); ++j)
                std::cout << "\t\tsubmatch " << j << ": " << base_match[j].str() << std::endl;
        }
    }

    return 0;
}
