#ifndef _ARGPARSE_H_
#define _ARGPARSE_H_

#include <string>
#include <vector>
#include <iostream>

namespace util {

class argparse {
public:
    class option {
    public:
        using _Self = option;

    public:
        _Self& required();
        _Self& value_holding();
        template <class String>
        _Self& default_value(String&& val);

    private:
        friend class argparse;
        std::string _value;
        std::string _default_value;
        bool _value_holding{false};
        bool _required{false};

    };
    using _Self = argparse;

public:
    argparse() noexcept = default;
    argparse(const _Self&) = delete;
    argparse(_Self&&) = delete;
    ~argparse() = default;
    _Self& operator=(const _Self&) = delete;
    _Self& operator=(_Self&&) = delete;

    option& add_option(char small_name);
    template <class String>
    option& add_option(char small_name, String&& long_name);
    template <class String>
    option& add_option(String&& long_name);

    template <class String>
    std::string& operator[](String&& name) const noexcept;
    std::string& operator[](char name) const noexcept;

    const std::vector<std::string>& free_args() const noexcept;
    std::string& operator[](std::size_t pos) const noexcept;

    void process();

private:

};

} // util

#endif // _ARGPARSE_H_
