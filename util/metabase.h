#ifndef _METABASE_H_
#define _METABASE_H_

#include <type_traits>
#include <utility>
#include <iterator>

namespace mtl {

template <class T, class = void>
struct is_iterator
    : public std::false_type {};
template <class T>
struct is_iterator<T, std::void_t<typename std::iterator_traits<T>::iterator_category>>
    : public std::true_type {};
template <class T>
constexpr bool is_iterator_v = is_iterator<T>::value;

template <class T, class Category, class = void>
struct is_iterator_of
    : public std::false_type {};
template <class T, class Category>
struct is_iterator_of<T, Category, std::void_t<typename std::iterator_traits<T>::iterator_category>>
    : public std::conditional_t<std::is_base_of<Category, typename std::iterator_traits<T>::iterator_category>::value, std::true_type, std::false_type> {};
template <class T, class Category>
constexpr bool is_iterator_of_v = is_iterator_of<T, Category>::value;

template <class T>
using is_input_iterator = is_iterator_of<T, std::input_iterator_tag>;
template <class T>
constexpr bool is_input_iterator_v = is_input_iterator<T>::value;

template <class T>
using is_output_iterator = is_iterator_of<T, std::output_iterator_tag>;
template <class T>
constexpr bool is_output_iterator_v = is_output_iterator<T>::value;

template <class T>
using is_forward_iterator = is_iterator_of<T, std::forward_iterator_tag>;
template <class T>
constexpr bool is_forward_iterator_v = is_forward_iterator<T>::value;

template <class T>
using is_bidirectional_iterator = is_iterator_of<T, std::bidirectional_iterator_tag>;
template <class T>
constexpr bool is_bidirectional_iterator_v = is_bidirectional_iterator<T>::value;

template <class T>
using is_random_access_iterator = is_iterator_of<T, std::random_access_iterator_tag>;
template <class T>
constexpr bool is_random_access_iterator_v = is_random_access_iterator<T>::value;

template <class T>
struct remove_cv_reference {
    using type = std::remove_reference_t<std::remove_cv_t<T>>;
};

template <class T>
using remove_cv_reference_t = typename remove_cv_reference<T>::type;

template <class F, class T, std::size_t ...Is>
inline decltype(auto) call_with_tuple_impl(F f, const T& t, std::index_sequence<Is...>) {
    return f(std::get<Is>(t)...);
}

template <class F, class ...Ts>
inline decltype(auto) call_with_tuple(F f, const std::tuple<Ts...>& t) {
    return call_with_tuple_impl(f, t, std::make_index_sequence<sizeof...(Ts)>());
}

} // mtl

#endif // _METABASE_H_
