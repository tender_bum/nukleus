#include "datetime.h"

namespace NDateTime {

// CTimeDelta

CTimeDelta::CTimeDelta(i64 val)
    : _data(val)
{
}

CTimeDelta CTimeDelta::operator +(const CTimeDelta& td) const {
    return CTimeDelta(_data + td._data);
}

CTimeDelta& CTimeDelta::operator +=(const CTimeDelta& td) {
    _data += td._data;
    return *this;
}

CTimeDelta CTimeDelta::operator -(const CTimeDelta& td) const {
    return CTimeDelta(_data - td._data);
}

CTimeDelta& CTimeDelta::operator -=(const CTimeDelta& td) {
    _data -= td._data;
    return *this;
}

CTimeDelta& CTimeDelta::set(i64 val, EDeltaType type) {
    switch (type) {
    case EDeltaType::SECONDS: return set_seconds(val);
    case EDeltaType::MINUTES: return set_minutes(val);
    case EDeltaType::HOURS:   return set_hours(val);
    case EDeltaType::DAYS:    return set_days(val);
    case EDeltaType::WEEKS:   return set_weeks(val);
    }
}

CTimeDelta& CTimeDelta::set_seconds(i64 val) {
    _data = val;    
    return *this;
}
CTimeDelta& CTimeDelta::set_minutes(i64 val) {
    _data = val * SECS_IN_MIN;
    return *this;
}
CTimeDelta& CTimeDelta::set_hours(i64 val) {
    _data = val * (MINS_IN_HOUR * SECS_IN_MIN);
    return *this;
}
CTimeDelta& CTimeDelta::set_days(i64 val) {
    _data = val * (HOURS_IN_DAY * MINS_IN_HOUR * SECS_IN_MIN);
    return *this;
}
CTimeDelta& CTimeDelta::set_weeks(i64 val) {
    _data = val * (DAYS_IN_WEEK * HOURS_IN_DAY * MINS_IN_HOUR * SECS_IN_MIN);
    return *this;
}

CTimeDelta& CTimeDelta::add(i64 val, EDeltaType type) {
    switch (type) {
    case EDeltaType::SECONDS: return add_seconds(val);
    case EDeltaType::MINUTES: return add_minutes(val);
    case EDeltaType::HOURS:   return add_hours(val);
    case EDeltaType::DAYS:    return add_days(val);
    case EDeltaType::WEEKS:   return add_weeks(val);
    }
}

CTimeDelta& CTimeDelta::add_seconds(i64 val) {
    _data += val;
    return *this;
}
CTimeDelta& CTimeDelta::add_minutes(i64 val) {
    _data += val * SECS_IN_MIN;
    return *this;
}
CTimeDelta& CTimeDelta::add_hours(i64 val) {
    _data += val * (MINS_IN_HOUR * SECS_IN_MIN);
    return *this;
}
CTimeDelta& CTimeDelta::add_days(i64 val) {
    _data += val * (HOURS_IN_DAY * MINS_IN_HOUR * SECS_IN_MIN);
    return *this;
}
CTimeDelta& CTimeDelta::add_weeks(i64 val) {
    _data += val * (DAYS_IN_WEEK * HOURS_IN_DAY * MINS_IN_HOUR * SECS_IN_MIN);
    return *this;
}

} // NDateTime
