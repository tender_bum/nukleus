#include <util/datetime/datetime.h>

#include <iostream>

int main() {
    auto td = NDateTime::CTimeDelta{}.add_days(2);
    std::cout << td.hours() << std::endl;
    td.add_minutes(30);
    std::cout << td.minutes() << std::endl;
    std::cout << td.hours() << std::endl;

    return 0;
}
