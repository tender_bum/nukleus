#ifndef _DATETIME_H_
#define _DATETIME_H_

#include <util/types.h>

namespace NDateTime {

constexpr i64 SECS_IN_MIN  = 60;
constexpr i64 MINS_IN_HOUR = 60;
constexpr i64 SECS_IN_HOUR = SECS_IN_MIN * MINS_IN_HOUR;
constexpr i64 HOURS_IN_DAY = 24;
constexpr i64 SECS_IN_DAY  = SECS_IN_HOUR * HOURS_IN_DAY;
constexpr i64 DAYS_IN_WEEK = 7;
constexpr i64 SECS_IN_WEEK = SECS_IN_DAY * DAYS_IN_WEEK;

class CTimeStamp;
class CTimeDelta;

enum class EStampType {
    SECOND,
    MINUTE,
    HOUR,
    DAY,
    MONTH,
    YEAR
};

class CTimeStamp {
public:
    CTimeStamp() = default;

    CTimeStamp operator +(const CTimeDelta& td) const;
    CTimeStamp& operator +=(const CTimeDelta& td);
    CTimeStamp operator -(const CTimeDelta& td) const;
    CTimeStamp& operator -=(const CTimeDelta& td);

    CTimeDelta operator -(const CTimeStamp& ts) const;

    bool operator ==(const CTimeStamp& td) const noexcept;
    bool operator !=(const CTimeStamp& td) const noexcept;
    bool operator <(const CTimeStamp& td) const noexcept;
    bool operator >(const CTimeStamp& td) const noexcept;
    bool operator <=(const CTimeStamp& td) const noexcept;
    bool operator >=(const CTimeStamp& td) const noexcept;

    CTimeStamp& set(i64 val, EStampType type);

    CTimeStamp& set_second(i64 val);
    CTimeStamp& set_minute(i64 val);
    CTimeStamp& set_hour(i64 val);
    CTimeStamp& set_day(i64 val);
    CTimeStamp& set_month(i64 val);
    CTimeStamp& set_year(i64 val);

    i64 get(EStampType type);

    i64 second() const noexcept;
    i64 minute() const noexcept;
    i64 hour() const noexcept;
    i64 day() const noexcept;
    i64 month() const noexcept;
    i64 year() const noexcept;
    
private:
    i64 _data{0}; // seconds

};

enum class EDeltaType {
    SECONDS,
    MINUTES,
    HOURS,
    DAYS,
    WEEKS
};

// Time delta holder. Use it to shift CTimeStamp
class CTimeDelta {
public:
    CTimeDelta() = default;
    CTimeDelta(i64 val);

    CTimeDelta operator +(const CTimeDelta& td) const;
    CTimeDelta& operator +=(const CTimeDelta& td);
    CTimeDelta operator -(const CTimeDelta& td) const;
    CTimeDelta& operator -=(const CTimeDelta& td);

    inline bool operator ==(const CTimeDelta& td) const noexcept {
        return _data == td._data;
    }
    inline bool operator !=(const CTimeDelta& td) const noexcept {
        return _data != td._data;
    }
    inline bool operator <(const CTimeDelta& td) const noexcept {
        return _data < td._data;
    }
    inline bool operator >(const CTimeDelta& td) const noexcept {
        return _data > td._data;
    }
    inline bool operator <=(const CTimeDelta& td) const noexcept {
        return _data <= td._data;
    }
    inline bool operator >=(const CTimeDelta& td) const noexcept {
        return _data >= td._data;
    }

    /* Setter methods. Resets the value.
     * To set complecated periods
     * like: "2 days 1 hour 25 seconds"
     * use "add" methods
     * like: "CTimeDelta().set_days(2).add_hours(1).add_seconds(25)"
     * or
     * like: "CTimeDelta(25).add_days(2).add_hours(1)" */
    CTimeDelta& set(i64 val, EDeltaType type);

    CTimeDelta& set_seconds(i64 val);
    CTimeDelta& set_minutes(i64 val);
    CTimeDelta& set_hours(i64 val);
    CTimeDelta& set_days(i64 val);
    CTimeDelta& set_weeks(i64 val);

    CTimeDelta& add(i64 val, EDeltaType type);

    CTimeDelta& add_seconds(i64 val);
    CTimeDelta& add_minutes(i64 val);
    CTimeDelta& add_hours(i64 val);
    CTimeDelta& add_days(i64 val);
    CTimeDelta& add_weeks(i64 val);

    inline i64 get(EDeltaType type) const noexcept {
        switch (type) {
        case EDeltaType::SECONDS: return seconds();
        case EDeltaType::MINUTES: return minutes();
        case EDeltaType::HOURS:   return hours();
        case EDeltaType::DAYS:    return days();
        case EDeltaType::WEEKS:   return weeks();
        }
    }

    inline i64 seconds() const noexcept {
        return _data;
    }
    inline i64 minutes() const noexcept {
        return _data / SECS_IN_MIN;
    }
    inline i64 hours() const noexcept {
        return _data / SECS_IN_HOUR;
    }
    inline i64 days() const noexcept {
        return _data / SECS_IN_DAY;
    }
    inline i64 weeks() const noexcept {
        return _data / SECS_IN_WEEK;
    }

private:
    i64 _data{0}; // seconds

};

} // NDateTime

#endif // _DATETIME_H_
