#include "logger.h"

#include <cstdio>
#include <iomanip>
#include <ctime>

namespace util {

inline std::string level_to_string(logger::level lvl) {
    switch (lvl) {
    case(logger::INFO):     return "INFO";
    case(logger::DEBUG):    return "DEBUG";
    case(logger::WARNING):  return "WARN";
    case(logger::ERROR):    return "ERROR";
    default: return "UNDEF";
    }
}

logger::logger(level lvl)
    : _os()
    , _lvl(lvl)
{
    using std::chrono::system_clock; // TODO datetime integration
    auto cur_time = system_clock::to_time_t(system_clock::now());
    _os << " - " << std::put_time(std::localtime(&cur_time), "%F %T") << " [" << level_to_string(_lvl) << "]:\t";
}

logger::~logger() {
    _os << std::endl;
    fprintf(stderr, "%s", _os.str().c_str());
    fflush(stderr);
}

} // util
