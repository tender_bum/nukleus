#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <ctime>
#include <sstream>
#include <chrono>
#include <atomic>

namespace util {

#define INFO_LOG()    util::logger(util::logger::INFO)
#define LOG()         INFO_LOG()
#define DEBUG_LOG()   util::logger(util::logger::DEBUG)
#define WARNING_LOG() util::logger(util::logger::WARNING)
#define ERROR_LOG()   util::logger(util::logger::ERROR)


class logger {
public:
    enum level {
        INFO,
        DEBUG,
        WARNING,
        ERROR
    };
    using _Self = logger;

public:
    logger(level lvl = INFO);
    logger(const _Self&) = delete;
    logger(_Self&&) = delete;
    ~logger();

    _Self& operator =(const _Self&) = delete;
    _Self& operator =(_Self&&) = delete;

    template <class T>
    _Self& operator <<(T&& data) {
        _os << std::forward<T>(data);
        return *this;
    }

private:
    std::ostringstream _os;
    level _lvl;

};

} // util

#endif // _LOGGER_H_
