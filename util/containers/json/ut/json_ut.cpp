#include <util/containers/json/json.h>

#include <iostream>

int main() {
    util::json the_undef;
    std::cout << the_undef.to_str() << '\n';
    std::cout << the_undef.get_type_name() << '\n';

    util::json the_map(util::json::T_MAP);
    std::cout << the_map.to_str() << '\n';
    std::cout << the_map.get_type_name() << '\n';

    util::json the_array(util::json::T_ARRAY);
    std::cout << the_array.to_str() << '\n';
    std::cout << the_array.get_type_name() << '\n';

    util::json the_null(util::json::T_NULL);
    std::cout << the_null.to_str() << '\n';
    std::cout << the_null.get_type_name() << '\n';

    util::json the_str("hello");
    std::cout << the_str.to_str() << '\n';
    std::cout << the_str.get_type_name() << '\n';

    util::json the_int(5);
    std::cout << the_int.to_str() << '\n';
    std::cout << the_int.get_type_name() << '\n';

    util::json the_uint((unsigned int)5);
    std::cout << the_uint.to_str() << '\n';
    std::cout << the_uint.get_type_name() << '\n';

    util::json the_bool(true);
    std::cout << the_bool.to_str() << '\n';
    std::cout << the_bool.get_type_name() << '\n';

    util::json the_double(5.0);
    std::cout << the_double.to_str() << '\n';
    std::cout << the_double.get_type_name() << '\n';

    the_map["key"] = "value";
    std::cout << the_map.to_str() << '\n';
    auto tmp = the_map;
    the_map["underkey"] = tmp;
    std::cout << the_map.to_str() << '\n';

    the_array[2] = "value";
    std::cout << the_array.to_str() << '\n';
    auto tmp2 = the_array;
    the_array[1] = tmp2;
    std::cout << the_array.to_str() << '\n';

    the_map["array"] = the_array;
    std::cout << the_map.to_str() << '\n';

    the_array[0] = the_map;
    std::cout << the_array.to_str() << '\n';

    util::json the_map2(util::json::T_MAP);
    std::cout << the_map2.to_str() << '\n';
    the_map2.get_by_path("key1/key2/key3", "/") = 5;
    std::cout << the_map2.to_str() << '\n';
    std::cout << the_map2.to_str(", ", ": ") << '\n';

    std::cout << the_map.to_str(", ", ": ") << '\n';
    std::cout << the_array.to_str(", ", ": ") << '\n';

    return 0;
}
