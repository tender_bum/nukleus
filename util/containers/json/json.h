#ifndef _JSON_H_
#define _JSON_H_

#include <util/types.h>

#include <string_view>
#include <unordered_map>
#include <vector>

namespace util {

class json {
public:
    enum content_type {
        T_INT,      // 0
        T_UINT,     // 1
        T_DOUBLE,   // 2
        T_BOOL,     // 3
        T_STRING,   // 4
        T_MAP,      // 5
        T_ARRAY,    // 6
        T_NULL,     // 7
        T_UNDEFINED // 8
    };

    using _Self = json;
    using dict_type = std::unordered_map<std::string, _Self>;
    using list_type = std::vector<_Self>;

public:
    json(content_type type = content_type::T_UNDEFINED);
    template <class T, class = std::enable_if_t<
        !std::is_same_v<std::decay_t<T>, _Self> &&
        !std::is_same_v<std::decay_t<T>, content_type>
    >>
    json(T&& val) {
        if constexpr (std::is_same_v<std::decay_t<T>, bool>) {
            _type = T_BOOL;
            _value._bool = val;
        } else if constexpr (std::is_floating_point_v<std::decay_t<T>>) {
            _type = T_DOUBLE;
            _value._double = val;
        } else if constexpr (std::is_signed_v<std::decay_t<T>>) {
            _type = T_INT;
            _value._int = val;
        } else if constexpr (std::is_unsigned_v<std::decay_t<T>>) {
            _type = T_UINT;
            _value._uint = val;
        } else if constexpr (std::is_constructible_v<std::string, T>) {
            _type = T_STRING;
            _value._string = new std::string(std::forward<T>(val));
        } else
            _type = T_UNDEFINED;
    }
    json(const _Self& val);
    json(_Self&& val);
    ~json();

    _Self &operator =(const _Self& val);
    _Self &operator =(_Self&& val);

    _Self operator [](std::size_t ind) const;
    _Self &operator [](std::size_t ind);
    _Self operator [](std::string_view key) const;
    _Self &operator [](std::string_view key);

    json get_by_path(std::string_view key, std::string_view separator = ".") const;
    json &get_by_path(std::string_view key, std::string_view separator = ".");

    void add_node(std::string_view key, const _Self& val);
    void add_node(std::string_view key, _Self&& val);

    inline content_type get_type() const {
        return _type;
    }

    std::string get_type_name() const;

    i64 get_int() const;
    i64 get_int(i64 plan_b) const;
    ui64 get_uint() const;
    ui64 get_uint(ui64 plan_b) const;
    double get_double() const;
    double get_double(double plan_b) const;
    bool get_bool() const;
    bool get_bool(bool plan_b) const;
    std::string get_string() const;
    std::string get_string(const std::string& plan_b) const;

    std::string to_str(std::string_view comma = ",", std::string_view colon = ":") const;

    void swap(_Self& rhs);

private:
    typedef union value_type {
        i64          _int;
        ui64         _uint;
        double       _double;
        bool         _bool;
        std::string* _string;
        dict_type*   _map;
        list_type*   _array;
        value_type() = default;
        ~value_type() = default;
    } value_type;

private:
    value_type _value;
    content_type _type;

private:
    void clear();

};

} // util

#endif // _JSON_H_
