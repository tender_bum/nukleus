#include "json.h"

#include <util/algo.h>

#include <numeric>

namespace util {

json::json(content_type type)
    : _type(type)
{
    switch (_type) {
    case T_STRING:
        _value._string = new std::string;
        break;
    case T_MAP:
        _value._map = new dict_type;
        break;
    case T_ARRAY:
        _value._array = new list_type;
        break;
    default:
        break;
    }
}

json::json(const _Self& val)
    : _type(val._type)
{
    switch (_type) {
    case T_STRING:
        _value._string = new std::string(*val._value._string);
        break;
    case T_MAP:
        _value._map = new dict_type(*val._value._map);
        break;
    case T_ARRAY:
        _value._array = new list_type(*val._value._array);
        break;
    default:
        _value = val._value;
    }
}

json::json(_Self&& val)
    : _type(val._type)
{
    val._type = T_UNDEFINED;
    _value = val._value;
}

json::~json() {
    clear();
}

json& json::operator =(const _Self& val) {
    _Self tmp(val);
    swap(tmp);
    return *this;
}

json& json::operator =(_Self&& val) {
    _Self tmp(std::move(val));
    swap(tmp);
    return *this;
}

json json::operator [](std::size_t ind) const {
    if (_type != T_ARRAY || _value._array->size() <= ind)
        return {};
    return (*_value._array)[ind];
}

json& json::operator [](std::size_t ind) {
    if (_type != T_ARRAY) {
        _Self tmp(T_ARRAY);
        swap(tmp); 
    }
    if (_value._array->size() <= ind)
        _value._array->resize(ind + 1);
    return (*_value._array)[ind];
}

json json::operator [](std::string_view key) const {
    if (_type != T_MAP)
        return {};
    auto it = _value._map->find(std::string(key));
    if (it == std::end(*_value._map))
        return {};
    return it->second;
}

json& json::operator [](std::string_view key) {
    if (_type != T_MAP) {
        _Self tmp(T_MAP);
        swap(tmp); 
    }
    return (*_value._map)[std::string(key)];
}

json json::get_by_path(std::string_view key, std::string_view separator) const {
    if (_type != T_MAP)
        return {};
    std::size_t pos = key.find(separator);
    if (pos == std::string_view::npos)
        return operator[](key);
    auto it = _value._map->find(std::string(key.substr(0, pos)));
    if (it == std::end(*_value._map))
        return {};
    return it->second.get_by_path(
        key.substr(pos + separator.length()),
        separator
    );
}

json& json::get_by_path(std::string_view key, std::string_view separator) {
    if (_type != T_MAP) {
        _Self tmp(T_MAP);
        swap(tmp);
    }
    std::size_t pos = key.find(separator);
    if (pos == std::string_view::npos)
        return operator[](key);
    return (*_value._map)[std::string(key.substr(0, pos))].get_by_path(
        key.substr(pos + separator.length()),
        separator
    );
}

void json::add_node(std::string_view key, const _Self& val) {
    if (_type != T_MAP) {
        _Self tmp(T_MAP);
        swap(tmp);
    }
    (*_value._map)[std::string(key)] = val;
}

void json::add_node(std::string_view key, _Self&& val) {
    if (_type != T_MAP) {
        _Self tmp(T_MAP);
        swap(tmp);
    }
    (*_value._map)[std::string(key)] = std::move(val);
}

std::string json::get_type_name() const {
    switch (_type) {
    case T_INT:       return "INT";
    case T_UINT:      return "UINT";
    case T_DOUBLE:    return "DOUBLE";
    case T_BOOL:      return "BOOL";
    case T_STRING:    return "STRING";
    case T_MAP:       return "MAP";
    case T_ARRAY:     return "ARRAY";
    case T_NULL:      return "NULL";
    case T_UNDEFINED: return "UNDEFINED";
    default:          return "UNDEFINED";
    }
}

i64 json::get_int() const {
    if (_type == T_INT)
        return _value._int;
    return {};
}
i64 json::get_int(i64 plan_b) const {
    if (_type == T_INT)
        return _value._int;
    return plan_b;
}
ui64 json::get_uint() const {
    if (_type == T_UINT)
        return _value._uint;
    return {};
}
ui64 json::get_uint(ui64 plan_b) const {
    if (_type == T_UINT)
        return _value._uint;
    return plan_b;
}
double json::get_double() const {
    if (_type == T_DOUBLE)
        return _value._double;
    return {};
}
double json::get_double(double plan_b) const {
    if (_type == T_DOUBLE)
        return _value._double;
    return plan_b;
}
bool json::get_bool() const {
    if (_type == T_BOOL)
        return _value._bool;
    return {};
}
bool json::get_bool(bool plan_b) const {
    if (_type == T_BOOL)
        return _value._bool;
    return plan_b;
}
std::string json::get_string() const {
    if (_type == T_STRING)
        return *_value._string;
    return {};
}
std::string json::get_string(const std::string& plan_b) const {
    if (_type == T_STRING)
        return *_value._string;
    return plan_b;
}

std::string json::to_str(std::string_view comma, std::string_view colon) const {
    switch (_type) {
    case T_INT:
        return std::to_string(_value._int);
    case T_UINT:
        return std::to_string(_value._uint);
    case T_DOUBLE:
        return std::to_string(_value._double);
    case T_BOOL:
        return _value._bool ? "true" : "false";
    case T_STRING:
        return "\"" + *_value._string + "\"";
    case T_MAP:
        return "{" + util::join(_value._map->begin(), _value._map->end(), comma, [comma, colon](const auto& el) {
            return "\"" + std::string(el.first) + "\"" + std::string(colon) + el.second.to_str(comma, colon);
        }) + "}";
    case T_ARRAY:
        return "[" + util::join(_value._array->begin(), _value._array->end(), comma, [comma, colon](const auto& el) {
            return el.to_str(comma, colon);
        }) + "]";
    case T_NULL:
        return "null";
    case T_UNDEFINED:
        return {};
    default:
        return {};
    }
}

void json::swap(_Self& rhs) {
    std::swap(_type, rhs._type);
    std::swap(_value, rhs._value);
}

void json::clear() {
    switch (_type) {
    case T_STRING:
        delete _value._string;
        break;
    case T_MAP:
        delete _value._map;
        break;
    case T_ARRAY:
        delete _value._array;
        break;
    default:
        break;
    }
    _type = T_UNDEFINED;
}

} // util
