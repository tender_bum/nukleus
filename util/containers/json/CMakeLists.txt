set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(json)

set(SOURCE_LIB json.cpp json.h)

add_library(json STATIC ${SOURCE_LIB})

add_subdirectory(ut)
