#include "util/containers/colony/colony.h"

#include <iostream>
#include <string>
#include <cassert>
#include <vector>
#include <list>

struct Tmp {
    int a;
    static int ctor_calls, dtor_calls;
    static void reset_calls_count() {ctor_calls = dtor_calls = 0;}
    Tmp() {std::cout << "default ctor(" << this << ") called\n";++ctor_calls;}
    Tmp(const Tmp& tmp) : a(tmp.a) {std::cout << "copy ctor(" << this << ") called\n";++ctor_calls;}
    Tmp(Tmp&& tmp) noexcept : a(tmp.a) {std::cout << "move ctor(" << this << ") called\n";++ctor_calls;}
    bool operator==(const Tmp& tmp) const {std::cout << "equality operator(" << this << ") called\n";return a == tmp.a;}
    Tmp(int t) : a(t) {std::cout << "int ctor(" << this << ") called\n";++ctor_calls;}
    Tmp(int t, int p) : a(t + p) {std::cout << "int, int ctor(" << this << ") called\n";++ctor_calls;}
    ~Tmp() {std::cout << "dtor(" << this << ") called\n";++dtor_calls;}
};

int Tmp::ctor_calls = 0;
int Tmp::dtor_calls = 0;

template <class F>
void test_unit_impl(const char* name, F f) {
    std::cout << "Test unit: " << name << std::endl;
    std::cout << "------------------------------" << std::endl;
    f();
    std::cout << "------------------------------" << std::endl;
}

template <class T>
void show_colony(const util::colony<T>& col) {
    for (const auto& e : col)
        std::cout << e << " ";
    std::cout << std::endl;
}

void show_colony(const util::colony<Tmp>& col) {
    for (const auto& e : col)
        std::cout << e.a << " ";
    std::cout << std::endl;
}

int main() {
    using util::colony;

    static_assert(std::is_same<colony<int>::iterator::_Self, colony<int>::iterator>());
    static_assert(std::is_same<colony<int>::_Self, colony<int>>());

    auto test_unit = [](const char* name, auto&& f) {
        Tmp::reset_calls_count();
        test_unit_impl(name, std::forward<decltype(f)>(f));
        assert(Tmp::dtor_calls == Tmp::ctor_calls);
        Tmp::reset_calls_count();
    };

    test_unit("Erase, clear and shrink_to_fit test", []{
        colony<Tmp> col(5, 2);

        assert(col.capacity() == 5);
        assert(col.size() == 5);
        int p = 1;
        for (Tmp& it : col) {
            it.a = p++;
            std::cout << it.a << " ";
        }
        std::cout << std::endl;

        std::cout << "Erasing..." << std::endl;
        auto it = col.erase(++col.begin());
        assert(col.capacity() == 5);
        assert(col.size() == 4);
        show_colony(col);

        std::cout << "Erasing..." << std::endl;
        col.erase(it);
        assert(col.capacity() == 5);
        assert(col.size() == 3);
        show_colony(col);

        std::cout << "Shrinking..." << std::endl;
        col.shrink_to_fit();
        assert(col.capacity() == 3);
        assert(col.begin() != col.end());
        show_colony(col);

        std::cout << "Erasing..." << std::endl;
        it = col.erase(col.begin());
        assert(col.capacity() == 3);
        assert(col.size() == 2);
        show_colony(col);

        std::cout << "Clearing..." << std::endl;
        col.clear();
        assert(col.capacity() == 0);
        assert(col.size() == 0);
        assert(col.empty());
    });

    test_unit("Equality test", []{
        std::cout << "colony(5, 3)..." << std::endl;
        colony<Tmp> col(5, 3);
        std::cout << "colony(5, 3)..." << std::endl;
        colony<Tmp> col2(5, 3);
        std::cout << "Comparing..." << std::endl;
        assert(col == col2);
        col.begin()->a = 2;
        std::cout << "Comparing..." << std::endl;
        assert(col != col2);
    });

    test_unit("Equality standart test", []{
        colony<Tmp> col(2, 1);
        auto col2(col);
        show_colony(col);
        show_colony(col2);
        assert(col == col2);
        assert(col == colony<Tmp>(col));
    });

    test_unit("Empty test", []{
        assert(colony<Tmp>().empty());
    });

    test_unit("Move test", []{
        colony<Tmp> col(3);
        auto col2(std::move(col));
    });

    test_unit("Equality size test", []{
        colony<Tmp> col(3);
        colony<Tmp> col2(6);
        assert(col != col2);
    });

    test_unit("Swap test", []{
        colony<Tmp> col(2);
        colony<Tmp> col2(3);

        assert(col.size() == 2 && col.capacity() == 2);
        assert(col2.size() == 3 && col2.capacity() == 3);

        std::cout << "col:" << std::endl;
        show_colony(col);
        std::cout << "col2:" << std::endl;
        show_colony(col2);

        std::cout << "Swapping..." << std::endl;
        col.swap(col2);

        assert(col.size() == 3 && col.capacity() == 3);
        assert(col2.size() == 2 && col2.capacity() == 2);

        std::cout << "col:" << std::endl;
        show_colony(col);
        std::cout << "col2:" << std::endl;
        show_colony(col2);
    });

    test_unit("Appending test", []{
        colony<Tmp> col;
        Tmp to_push(2);

        std::cout << "push_back(rvalue)..." << std::endl;
        col.push_back(Tmp(1));
        show_colony(col);
        assert(col.size() == 1);
        assert(col.capacity() == 2);

        std::cout << "push_back(lvalue)..." << std::endl;
        col.push_back(to_push);
        show_colony(col);
        assert(col.size() == 2);
        assert(col.capacity() == 2);

        std::cout << "push_back(rvalue)..." << std::endl;
        col.push_back(Tmp(1));
        show_colony(col);
        assert(col.size() == 3);
        assert(col.capacity() == 6);

        std::cout << "emplace_back(5)..." << std::endl;
        col.emplace_back(5);
        show_colony(col);
        assert(col.size() == 4);
        assert(col.capacity() == 6);

        std::cout << "push_back(3)..." << std::endl;
        col.push_back(3);
        show_colony(col);
        assert(col.size() == 5);
        assert(col.capacity() == 6);

        std::cout << "emplace_back(6, 7)..." << std::endl;
        col.emplace_back(6, 7);
        show_colony(col);
        assert(col.size() == 6);
        assert(col.capacity() == 6);
    });

    test_unit("Erase range test", []{
        colony<Tmp> col;
        std::cout << "emplace_back(3)..." << std::endl;
        col.emplace_back(3);
        std::cout << "emplace_back(4, 1)..." << std::endl;
        col.emplace_back(4, 1);
        std::cout << "emplace_back(2)..." << std::endl;
        col.emplace_back(2);
        std::cout << "emplace_back(6, 2)..." << std::endl;
        col.emplace_back(6, 2);
        show_colony(col);
        assert(col.size() == 4);
        assert(col.capacity() == 6);

        std::cout << "Erasing..." << std::endl;
        col.erase(++col.begin(), ++(++col.begin()));
        show_colony(col);
        assert(col.size() == 3);
        assert(col.capacity() == 6);

        std::cout << "Erasing..." << std::endl;
        col.erase(++col.begin(), col.end());
        show_colony(col);
        assert(col.size() == 1);
        assert(col.capacity() == 1); // optimization happend
    });

    test_unit("InputIt test", []{
        std::vector<Tmp> vec(5);
        std::cout << "vector:" << std::endl;
        for (const auto& elem : vec)
            std::cout << elem.a << " ";
        std::cout << std::endl;
        colony<Tmp> col(vec.begin(), vec.end());
        std::cout << "colony:" << std::endl;
        show_colony(col);
        assert(col.size() == 5);
        assert(col.capacity() == 5);
    });

    test_unit("InputIt test 2", []{
        std::vector<Tmp> vec;
        for (int i = 0; i < 5; ++i)
            vec.emplace_back(i);
        std::cout << "vector:" << std::endl;
        for (const auto& elem : vec)
            std::cout << elem.a << " ";
        std::cout << std::endl;
        colony<Tmp> col(++vec.begin(), --vec.end());
        std::cout << "colony:" << std::endl;
        show_colony(col);
        assert(col.size() == 3);
        assert(col.capacity() == 3);
    });

    test_unit("initializer list test", []{
        colony<Tmp> col{1, 2};
        show_colony(col);
        assert(col.size() == 2);
        assert(col.capacity() == 2);
        colony<Tmp> col2(col.begin(), ++col.begin());
        show_colony(col2);
        assert(col2.size() == 1);
        assert(col2.capacity() == 1);
    });

    test_unit("assign test", []{
        std::vector<Tmp> vec(5, 2);
        colony<Tmp> col(3, 1);
        assert(col.size() == 3);
        assert(col.capacity() == 3);
        show_colony(col);

        col.assign(vec.begin(), vec.end());
        assert(col.size() == 5);
        assert(col.capacity() == 5);
        show_colony(col);

        col.assign(8, Tmp(1, 5));
        assert(col.size() == 8);
        assert(col.capacity() == 8);
        show_colony(col);

        col.assign(++vec.begin(), --vec.end());
        assert(col.size() == 3);
        assert(col.capacity() == 3);
        show_colony(col);
    });

    return 0;
};
