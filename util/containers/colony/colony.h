#ifndef _COLONY_H_
#define _COLONY_H_

#include <util/metabase.h>

#include <type_traits>
#include <memory>
#include <atomic>

namespace util {

template <class T>
class colony {
public:
    using value_type = T;
    using reference = T&;
    using const_reference = const T&;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using _Self = colony;

private:
    using pointer = T*;
    using const_pointer = const T*;

private:
    class data_cage {
    public:
        using _Self = data_cage;

    public:
        data_cage()
            : _next(this)
        {
        }
        ~data_cage() {
            if (alive())
                destruct();
        }
        _Self* get_nearest() const {
            if (this == _next)
                return _next;
            return _next = _next->get_nearest();
        }
        bool alive() const {
            return this == _next;
        }
        reference get_value() {
            return reinterpret_cast<reference>(_data);
        }
        const_reference get_value() const {
            return reinterpret_cast<const_reference>(_data);
        }
        template <class ...Args>
        void construct(Args&&... args) {
            new (&_data) value_type(std::forward<Args>(args)...);
        }
        void destruct() noexcept {
            get_value().~value_type();
        }
        _Self* kill() {
            destruct();
            return _next = (this + 1)->get_nearest();
        }

    private:
        std::aligned_storage_t<sizeof(value_type)> _data;
        mutable _Self* _next;

    };

public:
    class const_iterator {
    public:
        using value_type = T;
        using reference = const T&;
        using pointer = const T*;
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::forward_iterator_tag;
        using _Self = const_iterator;
    private:
        using real_value_type = const data_cage*;
    public:
        const_iterator() = default;
        const_iterator(real_value_type ptr)
            : _ptr(ptr)
        {
        }
        _Self& operator ++() {
            _ptr = (_ptr + 1)->get_nearest();
            return *this;
        }
        _Self operator ++(int) {
            auto tmp = *this;
            _ptr = (_ptr + 1)->get_nearest();
            return tmp;
        }
        reference operator *() const {
            return _ptr->get_value();
        }
        pointer operator ->() const {
            return &_ptr->get_value();
        }
        bool operator ==(const _Self& it) const {
            return _ptr == it._ptr;
        }
        bool operator !=(const _Self& it) const {
            return !operator==(it);
        }
    private:
        friend class colony;
        real_value_type _ptr{nullptr};
    };

    class iterator {
    public:
        using value_type = T;
        using reference = T&;
        using pointer = T*;
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::forward_iterator_tag;
        using _Self = iterator;
    private:
        using real_value_type = data_cage*;
    public:
        iterator() = default;
        iterator(real_value_type ptr)
            : _ptr(ptr)
        {
        }
        _Self& operator ++() {
            _ptr = (_ptr + 1)->get_nearest();
            return *this;
        }
        _Self operator ++(int) {
            auto tmp = *this;
            _ptr = (_ptr + 1)->get_nearest();
            return tmp;
        }
        reference operator *() const {
            return _ptr->get_value();
        }
        pointer operator ->() const {
            return &_ptr->get_value();
        }
        bool operator ==(const _Self& it) const {
            return _ptr == it._ptr;
        }
        bool operator !=(const _Self& it) const {
            return !operator==(it);
        }
        operator const_iterator() const {
            return _ptr;
        }
    private:
        friend class colony;
        real_value_type _ptr{nullptr};
    };

private:
    size_type active_size() const {
        return _end - _begin;
    }

public:
    colony()
        : _holes_count(0)
        , _begin(_allocator.allocate(1))
        , _end(_begin)
        , _real_end(_end)
    {
        new (_begin) data_cage;
    }
    colony(size_type len)
        : _holes_count(0)
        , _begin(_allocator.allocate(len + 1))
        , _end(_begin + len)
        , _real_end(_end)
    {
        static_assert(std::is_default_constructible<value_type>(), "value_type can't be default constructed");
        for (auto ptr = _begin; ptr < _end; ++ptr) {
            new (ptr) data_cage;
            ptr->construct();
        }
        new (_end) data_cage;
    }
    colony(size_type len, const_reference def_val)
        : _holes_count(0)
        , _begin(_allocator.allocate(len + 1))
        , _end(_begin + len)
        , _real_end(_end)
    {
        static_assert(std::is_copy_constructible<value_type>(), "value_type can't be copy constructed");
        for (auto ptr = _begin; ptr < _end; ++ptr) {
            new (ptr) data_cage;
            ptr->construct(def_val);
        }
        new (_end) data_cage;
    }
    template <class InputIt, class = std::enable_if_t<mtl::is_input_iterator_v<InputIt>>>
    colony(InputIt i, InputIt j)
        : _holes_count(0)
    {
        auto len = std::distance(i, j);
        _begin = _allocator.allocate(len + 1);
        _real_end = _end = _begin + len;
        auto ptr = _begin;
        while (i != j) {
            new (ptr) data_cage;
            (ptr++)->construct(*(i++));
        }
        new (_end) data_cage;
    }
    colony(std::initializer_list<value_type> il)
        : colony(il.begin(), il.end())
    {
    }
    colony(const _Self& rhs)
        : _holes_count(0)
        , _begin(_allocator.allocate(rhs.size() + 1))
        , _end(_begin + rhs.size())
        , _real_end(_end)
    {
        static_assert(std::is_copy_constructible<value_type>(), "value_type can't be copy constructed");
        auto ptr = _begin;
        for (const auto& elem : rhs) {
            new (ptr) data_cage;
            (ptr++)->construct(elem);
        }
        new (_end) data_cage;
    }
    colony(_Self&& rhs) noexcept
        : _holes_count(rhs._holes_count)
        , _begin(rhs._begin)
        , _end(rhs._end)
        , _real_end(rhs._real_end)
    {
        rhs._holes_count = 0;
        rhs._real_end = rhs._end = rhs._begin = rhs._allocator.allocate(1);
        new (rhs._begin) data_cage;
    }
    ~colony() {
        std::destroy(begin(), end());
        _allocator.deallocate(_begin, capacity() + 1);
    }
    _Self& operator=(const _Self& rhs) { // TODO no allocate case
        _Self tmp(rhs);
        swap(tmp);
    }
    _Self& operator=(_Self&& rhs) noexcept {
        _Self tmp(std::move(rhs));
        swap(tmp);
    }

    _Self& operator=(std::initializer_list<value_type> il) { // TODO no allocate case
        _Self tmp(il);
        swap(tmp);
    }

    bool operator==(const _Self& rhs) const {
        return size() == rhs.size() && std::equal(begin(), end(), rhs.begin());
    }

    bool operator!=(const _Self& rhs) const {
        return !operator==(rhs);
    }

    size_type size() const noexcept {
        return (_end - _begin) - _holes_count;
    }

    size_type capacity() const noexcept {
        return _real_end - _begin;
    }

    void swap(_Self& rhs) noexcept {
        std::swap(_holes_count, rhs._holes_count);
        std::swap(_begin, rhs._begin);
        std::swap(_end, rhs._end);
        std::swap(_real_end, rhs._real_end);
    }

    iterator begin() {
        return _begin->get_nearest();
    }

    const_iterator begin() const {
        return _begin->get_nearest();
    }

    const_iterator cbegin() const {
        return _begin->get_nearest();
    }

    iterator end() {
        return _end;
    }

    const_iterator end() const {
        return _end;
    }

    const_iterator cend() const {
        return _end;
    }

    bool empty() const noexcept {
        return size() == 0;
    }

    iterator erase(const_iterator it) {
        auto nit = _begin + (it._ptr - _begin);
        ++_holes_count;
        return nit->kill();
    }

    iterator erase(const_iterator p, const_iterator q) {
        auto s_ptr = _begin + (p._ptr - _begin);
        auto e_ptr = _begin + (q._ptr - _begin);
        _holes_count += e_ptr - s_ptr;
        while (s_ptr < e_ptr)
            s_ptr = s_ptr->kill();
        try_optimize();
        return e_ptr;
    }

    void clear() {
        _Self tmp;
        swap(tmp);
    }

    void shrink_to_fit() {
        if (size() == capacity())
            return;
        realloc(size());
    }

    void push_back(const_reference val) {
        if (_end == _real_end)
            realloc((size() + 1) * 2);
        _end->construct(val);
        new (++_end) data_cage;
    }

    void push_back(value_type&& val) {
        if (_end == _real_end)
            realloc((size() + 1) * 2);
        _end->construct(std::move(val));
        new (++_end) data_cage;
    }

    template <class ...Args>
    void emplace_back(Args&&... args) {
        if (_end == _real_end)
            realloc((size() + 1) * 2);
        _end->construct(std::forward<Args>(args)...);
        new (++_end) data_cage;
    }

    reference front() {
        return *begin();
    }

    const_reference front() const {
        return *begin();
    }

    template <class InputIt>
    std::enable_if_t<mtl::is_input_iterator_v<InputIt>, void> assign(InputIt i, InputIt j) {
        _Self tmp(i, j);
        swap(tmp);
    }

    void assign(std::initializer_list<value_type> il) {
        _Self tmp(il);
        swap(tmp);
    }

    void assign(size_type len, const_reference val) {
        _Self tmp(len, val);
        swap(tmp);
    }

private:
    void realloc(size_type new_len) {
        auto new_begin = _allocator.allocate(new_len + 1);
        auto ptr = new_begin;
        for (auto& elem : *this) {
            new (ptr) data_cage;
            if constexpr (std::is_nothrow_move_constructible_v<value_type>)
                (ptr++)->construct(std::move(elem));
            else
                (ptr++)->construct(elem);
            elem.~value_type();
        }
        auto new_end = new_begin + size();
        new (new_end) data_cage;
        _holes_count = 0;
        _allocator.deallocate(_begin, capacity() + 1);
        _begin = new_begin;
        _end = new_end;
        _real_end = _begin + new_len;
    }

    void try_optimize() {
        if (size() * 2 < _holes_count)
            shrink_to_fit();
    }

private:
    std::allocator<data_cage> _allocator;
    size_type _holes_count;
    data_cage* _begin;
    data_cage* _end;
    data_cage* _real_end;

};

template <class T>
void swap(colony<T> &lhs, colony<T> &rhs) noexcept {
    lhs.swap(rhs);
}

} // util

#endif // _COLONY_H_
