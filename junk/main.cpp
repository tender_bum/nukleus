template <class T>
T min(T a) {
    return a;
}

template <class T, class ...Ts>
auto min(T a, Ts... as) {
    auto res = min(as...);
    return (a < res ? a : res);
}

#include <iostream>

int main() {
    std::cout << min(5, 1, 2.0);

    return 0;
}
