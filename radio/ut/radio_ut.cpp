#include <radio/signal.h>
#include <util/logger/logger.h>

#include <iostream>

class widget1
    : public radio::receiver
{
public:
    void bar() {
        LOG() << "called bar()";
        LOG() << "emitting signal1()...";
        emit signal1();
    }
    void bar2() {
        LOG() << "called bar2()";
        LOG() << "emitting signal2(5)...";
        emit signal2(5);
    }
    void bar3() {
        LOG() << "called bar3()";
    }

signals:
    radio::signal<> signal1;
    radio::signal<int> signal2;

};

class widget2
    : public radio::receiver
{
public:
    void foo() {
        LOG() << "called foo()";
    }
    void foo_int(int a) {
        LOG() << "called foo_int(" << a << ") (int)";
    }
    void foo_int(double a) {
        LOG() << "called foo_int(" << a << ") (double)";
    }

};

class widget3 {
public:
    void boo() {
        LOG() << "called boo()";
        LOG() << "emitting signal3()...";
        signal3();
    }

signals:
    radio::signal<> signal3;

};

int main() {
    widget1 a;
    widget2 b;
    widget3 c;

    a.signal1.mklink(b, [](widget2& w){
        w.foo();
    });
    a.signal2.mklink(b, [](widget2& w, double d){
        w.foo_int(d);
    });
    c.signal3.mklink(a, [](widget1& w){
        w.bar3();
    });
    c.signal3.mklink(a, [](widget1& w){
        w.bar2();
    });

    LOG() << "calling a.bar()...";
    a.bar();
    LOG() << "calling a.bar2()...";
    a.bar2();
    LOG() << "calling c.boo()...";
    c.boo();
    LOG() << "removing links: (c.signal3, a)";
    c.signal3.rmlinks(a);
    LOG() << "calling c.boo()...";
    c.boo();

    return 0;
};
