#ifndef _RECEIVER_H_
#define _RECEIVER_H_

namespace radio {

// The only class that can response to signals.
class receiver {
public:
    using _Self = receiver;

public:
    receiver() = default;
    receiver(_Self&&) = delete;
    receiver &operator =(_Self&&) = delete;
    virtual ~receiver() = 0;

};

receiver::~receiver() {}

} // radio

#endif // _RECEIVER_H_
