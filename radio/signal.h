#ifndef _SIGNAL_H_
#define _SIGNAL_H_

#include "receiver.h"

#include <functional>
#include <unordered_map>
#include <vector>

namespace radio {

#define signals public
#define emit

template <class ...Ts>
class signal {
public:
    using action = std::function<void(receiver&, const Ts&...)>;

public:
    void operator()(const Ts&... args) const {
        for (const auto& act_pair : _action_map)
            act_pair.second(*act_pair.first, args...);
    }

    template <class Receiver, class Action>
    void mklink(Receiver& rec, Action act) {
        static_assert(std::is_base_of_v<receiver, std::decay_t<Receiver>>);
        static_assert(std::is_invocable_v<Action, Receiver&, const Ts&...>);
        _action_map.insert({
            &rec,
            [act](receiver& rec, const Ts&... args){
                act(static_cast<Receiver&>(rec), args...);
            }
        });
    }

    template <class Receiver>
    void rmlinks(Receiver& rec) {
        static_assert(std::is_base_of_v<receiver, std::decay_t<Receiver>>);
        _action_map.erase(&rec);
    }

protected:
    std::unordered_multimap<receiver*, action> _action_map;

};

} // radio

#endif // _SIGNAL_H_
