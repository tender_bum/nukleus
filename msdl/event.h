#ifndef _EVENT_H_
#define _EVENT_H_

#include <SDL_events.h>

namespace msdl {

enum class event_type {
    UNDEFINED,
    QUIT,

    WINDOW_EVENT,
    SYSWM_EVENT,

    KEY_DOWN,
    KEY_UP,
    TEXT_EDITING,
    TEXT_INPUT,
    KEYMAP_CHANGED,

    MOUSE_MOTION,
    MOUSE_BTN_DOWN,
    MOUSE_BTN_UP,
    MOUSE_WHEEL,

    JOY_AXIS_MOTION,
    JOY_BALL_MOTION,
    JOY_HAT_MOTION,
    JOY_BTN_DOWN,
    JOY_BTN_UP,
    JOY_DVC_ADDED,
    JOY_DVC_REMOVED,

    CTL_AXIS_MOTION, 
    CTL_BTN_DOWN, 
    CTL_BTN_UP, 
    CTL_DVC_ADDED, 
    CTL_DVC_REMOVED, 
    CTL_DVC_REMAPPED, 

    FINGER_DOWN,
    FINGER_UP,
    FINGER_MOTION,

    DOLLAR_GESTURE,
    DOLLAR_RECORD,
    MULTI_GESTURE,

    CLIPBOARD_UPDATE,

    DROP_FILE,
    DROP_TEXT,
    DROP_BEGIN,
    DROP_COMPLETE,

    AUDIO_DVC_ADDED,
    AUDIO_DVC_REMOVED,

    RENDER_TARGETS_RESET,
    RENDER_DVC_RESET
};

// SDL_Event wrapper.
class event {
public:
    using _Self = event;

public:
    event() = default;
    event(const _Self&) = delete; // TODO?
    event(_Self&&) = default;
    event(const SDL_Event& e);
    event(SDL_Event&& e);
    ~event();

    _Self& operator =(const _Self&) = delete; // TODO?
    _Self& operator =(_Self&) = default;

    event_type get_type() const;
    inline const SDL_Event& get_data() const {
        return _data;
    }

public:
    SDL_Event _data;

};

} // msdl


#endif
