#ifndef _DRAWINGAREA_H_
#define _DRAWINGAREA_H_

#include "color.h"
#include "primitives.h"
#include "deleters.h"

#include <memory>

namespace msdl {

// Basic SDL_Renderer holder. Encapsulates render functions.
class drawing_area {
protected:
    using _Self = drawing_area;

public:
    drawing_area() = delete;
    drawing_area(const _Self&) = delete; // TODO
    drawing_area(_Self&&) = default;
    drawing_area(SDL_Renderer* renderer);
    virtual ~drawing_area() = 0;

    _Self& operator=(const _Self&) = delete; // TODO
    _Self& operator=(_Self&&) = default;

    // set_point
    inline void set_point(int x, int y, color col) {
        apply_color(col);
        set_point(x, y);
    }
    inline void set_point(point p, color col) {
        apply_color(col);
        set_point(p);
    }
    // Draw point using current renderer color
    void set_point(int x, int y);
    void set_point(point p);

    // set_line
    inline void set_line(int x1, int y1, int x2, int y2, color col) {
        apply_color(col);
        set_line(x1, y1, x2, y2);
    }
    inline void set_line(point p1, point p2, color col) {
        apply_color(col);
        set_line(p1, p2);
    }
    inline void set_line(const line& l, color col) {
        apply_color(col);
        set_line(l);
    }
    // Draw line using current renderer color
    void set_line(int x1, int y1, int x2, int y2);
    void set_line(point p1, point p2);
    void set_line(const line& l);

    // set_rect
    inline void set_rect(int x, int y, int w, int h, color col) {
        apply_color(col);
        set_rect(x, y, w, h);
    }
    inline void set_rect(const rect& r, color col) {
        apply_color(col);
        set_rect(r);
    }
    // Draw rect using current renderer color
    void set_rect(int x, int y, int w, int h);
    void set_rect(const rect& r);

    // fill_rect
    inline void fill_rect(int x, int y, int w, int h, color col) {
        apply_color(col);
        fill_rect(x, y, w, h);
    }
    inline void fill_rect(const rect& r, color col) {
        apply_color(col);
        fill_rect(r);
    }
    // Fill rect using current renderer color
    void fill_rect(int x, int y, int w, int h);
    void fill_rect(const rect& r);

    // TODO set_poly

    virtual void clear(color col = BLACK);

    int width() const;
    int height() const;

    void apply_color(color col);

protected:
    std::unique_ptr <SDL_Renderer, magic::sdl_deleter> _renderer;

protected:

};

} // msdl

#endif // _DRAWINGAREA_H_
