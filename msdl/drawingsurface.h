#ifndef _DRAWINGSURFACE_H_
#define _DRAWINGSURFACE_H_

#include "drawingarea.h"
#include "surfacewrap.h"

namespace msdl {

class drawing_surface
    : public surface_wrap
    , public drawing_area
{
public:
    drawing_surface() = delete;
    drawing_surface(const drawing_surface&) = delete; // TODO
    drawing_surface(drawing_surface&&) = default;

    drawing_surface(const char* path, file_format fmt = file_format::BMP);
    drawing_surface(const std::string& path, file_format fmt = file_format::BMP);

    drawing_surface(int w, int h);

    ~drawing_surface() override = default;

    drawing_surface &operator =(const drawing_surface&) = delete; // TODO
    drawing_surface &operator =(drawing_surface&&) = default;

};

} // msdl

#endif // _DRAWINGSURFACE_H_
