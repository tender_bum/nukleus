#ifndef _WINDOWWRAP_H_
#define _WINDOWWRAP_H_

#include "deleters.h"

#include <memory>

namespace msdl {

// Basic window wrapper.
class window_wrap {
public:
    using _Self = window_wrap;

public:
    window_wrap() = delete;
    window_wrap(const _Self&) = delete; // TODO
    window_wrap(_Self&&) = default;
    window_wrap(const char* title, int w, int h);
    window_wrap(const std::string& title, int w, int h);

    virtual ~window_wrap() = default;

    _Self& operator =(const _Self&) = delete; // TODO
    _Self& operator =(_Self&&) = default;

protected:
    std::unique_ptr <SDL_Window, magic::sdl_deleter> _window;

};

} // msdl

#endif // _WINDOWWRAP_H_
