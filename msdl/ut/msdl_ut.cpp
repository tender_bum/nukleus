#include <msdl/drawingsurface.h>
#include <msdl/drawingwindow.h>
#include <msdl/eventmanager.h>

#include <SDL.h>

#include <iostream>
#include <cassert>

void image_save_test() {

    msdl::drawing_surface dps(200, 200);

    dps.fill_rect(0, 0, 20, 20, msdl::RED);
    dps.fill_rect(20, 0, 20, 20, msdl::GREEN);
    dps.fill_rect(40, 0, 20, 20, msdl::BLUE);
    dps.save("tmp.bmp");

    dps.clear();
    dps.save("tmp2.bmp");

    dps.fill_rect(0, 0, 20, 20, msdl::RED);
    dps.fill_rect(5, 5, 10, 10, msdl::GREEN);
    dps.save("tmp3.bmp");

    dps.clear();

    msdl::rect r1(0, 0, 10, 10), r2(25, 30, 20, 35), r3(50, 50, 5, 5);

    dps.set_rect(r1, msdl::GREEN);
    dps.set_rect(r2, msdl::GREEN);
    dps.set_rect(r3, msdl::GREEN);
    dps.save("tmp4.bmp");

    dps.fill_rect(msdl::rect(20, 12, 15, 16), msdl::RED);
    dps.fill_rect(r1,  msdl::RED);
    dps.fill_rect(r3, msdl::RED);
    dps.save("tmp5.bmp");

}

void window_test() {
    msdl::drawing_window win("Test", 500, 500);
    msdl::event_manager emanager;
    bool quit = false;
    while (!quit) {
        while (!emanager.is_empty()) {
            auto e = emanager.get_next();
            if (e->get_type() == msdl::event_type::QUIT)
                quit = true;
        }
        win.clear();
        win.set_rect(0, 5, 40, 30, msdl::WHITE);
        win.refresh();
    }
}

void image_load_test() {
    msdl::drawing_surface surf("tmp4.bmp");
    assert(surf.width() == 200);
    assert(surf.height() == 200);
    surf.set_rect(5, 5, 20, 20, msdl::RED);
    surf.clear();
    surf.set_rect(5, 5, 20, 20, msdl::RED);
    surf.save("tmp6.bmp");
}

int main() {
    std::cout << SDL_Init(SDL_INIT_EVERYTHING) << std::endl;

    // window_test();

    image_save_test();

    image_load_test();

    SDL_Quit();

    return 0;
}
