set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(msdl)

set(SOURCE_LIB
    drawingarea.cpp     drawingarea.h
    surfacewrap.cpp     surfacewrap.h
    drawingsurface.cpp  drawingsurface.h
    windowwrap.cpp      windowwrap.h
    drawingwindow.cpp   drawingwindow.h
    event.cpp           event.h
    eventmanager.cpp    eventmanager.h
    primitives.cpp      primitives.h
    color.cpp           color.h
    deleters.cpp        deleters.h
)

add_library(msdl STATIC ${SOURCE_LIB})

target_link_libraries(msdl ${SDL2_LIBRARIES} logger)

add_subdirectory(ut)
