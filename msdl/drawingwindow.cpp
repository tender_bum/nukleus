#include "drawingwindow.h"

#include <SDL_render.h>

namespace msdl {

drawing_window::drawing_window(const char* title, int w, int h)
    : window_wrap(title, w, h)
    , drawing_area(SDL_CreateRenderer(_window.get(), 1, SDL_RENDERER_ACCELERATED))
{
}

drawing_window::drawing_window(const std::string& title, int w, int h)
    : drawing_window(title.c_str(), w, h)
{
}

void drawing_window::refresh() {
    SDL_RenderPresent(_renderer.get());
}

} // msdl
