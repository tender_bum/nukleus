#ifndef _COLOR_H_
#define _COLOR_H_

#include <SDL_pixels.h>

namespace msdl {

class color {
public:
    color() = default;
    color(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
        : r(r)
        , g(g)
        , b(b)
        , a(a)
    {
    }
    color(Uint32 color)
        : r(color >> 24)
        , g(color >> 16)
        , b(color >> 8)
        , a(color)
    {
    }
    color(SDL_Color color)
        : _sdl(color)
    {
    }

    operator SDL_Color() const {
        return _sdl;
    }

public:
    union {
        Uint8 _arr[4];
        SDL_Color _sdl;
        struct {
            Uint8 r, g, b, a;
        };
    };

};

const color BLACK = Uint32(0x000000ff);
const color WHITE = Uint32(0xffffffff);

const color RED   = Uint32(0xff0000ff);
const color GREEN = Uint32(0x00ff00ff);
const color BLUE  = Uint32(0x0000ffff);

} // msdl

#endif
