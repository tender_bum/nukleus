#include "drawingsurface.h"

#include <SDL_render.h>

namespace msdl {

drawing_surface::drawing_surface(const char* path, file_format fmt)
    : surface_wrap(path, fmt)
    , drawing_area(SDL_CreateSoftwareRenderer(_surface.get()))
{
}

drawing_surface::drawing_surface(const std::string& path, file_format fmt)
    : drawing_surface(path.c_str(), fmt)
{
}

drawing_surface::drawing_surface(int w, int h)
    : surface_wrap(w, h)
    , drawing_area(SDL_CreateSoftwareRenderer(_surface.get()))
{
}

} // msdl
