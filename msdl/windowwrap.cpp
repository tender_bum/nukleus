#include "windowwrap.h"

#include <util/logger/logger.h>

#include <SDL_render.h>

namespace msdl {

window_wrap::window_wrap(const char* title, int w, int h)
    : _window(SDL_CreateWindow(title, 0, 0, w, h, SDL_WINDOW_SHOWN))
{
    if (_window == nullptr)
        ERROR_LOG() << SDL_GetError();
    else
        INFO_LOG() << "window_wrap created";
}

window_wrap::window_wrap(const std::string& title, int w, int h)
    : window_wrap(title.c_str(), w, h)
{
}

} // msdl
