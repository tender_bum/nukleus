#include "eventmanager.h"

bool poll_event(SDL_Event* e) {
    return static_cast<bool>(SDL_PollEvent(e));
}

namespace msdl {

event_manager::event_manager()
    : _iter()
{
}

bool event_manager::is_empty() const {
    return !poll_event(nullptr);
}

const event* event_manager::get_next() {
    if (poll_event(&_iter._data))
        return &_iter;
    return nullptr;
}


} // msdl
