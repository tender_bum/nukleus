#ifndef _SURFACEWRAP_H_
#define _SURFACEWRAP_H_

#include "deleters.h"

#include <string>

#include <memory>

namespace msdl {

// Basic SDL_Surface holder that can save and load surface from file.
class surface_wrap {
public:
    using _Self = surface_wrap;

public:
    enum class file_format {
        BMP // TODO
    };

public:
    surface_wrap() = delete;
    surface_wrap(const _Self &) = delete; // TODO
    surface_wrap(surface_wrap &&) = default;

    surface_wrap(const char* path, file_format fmt = file_format::BMP);
    surface_wrap(const std::string& path, file_format fmt = file_format::BMP);

    surface_wrap(int w, int h);

    virtual ~surface_wrap() = default;

    surface_wrap &operator =(const _Self&) = delete; // TODO
    surface_wrap &operator =(_Self&&) = default;

    void save(const char* path, file_format fmt = file_format::BMP);
    void save(const std::string& path, file_format fmt = file_format::BMP);

protected:
    std::unique_ptr<SDL_Surface, magic::sdl_deleter> _surface;

};

} // msdl

#endif // _SURFACEWRAP_H_
