#ifndef _DELETERS_H_
#define _DELETERS_H_

class SDL_Surface;
class SDL_Window;
class SDL_Renderer;

namespace msdl::magic {

class sdl_deleter {
public:
    sdl_deleter() = default;
    void operator ()(SDL_Surface* p);
    void operator ()(SDL_Window* p);
    void operator ()(SDL_Renderer* p);

};

} // msdl::magic


#endif // _DELETERS_H_
