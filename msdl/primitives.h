#ifndef _PRIMITIVES_H_
#define _PRIMITIVES_H_

#include <SDL_rect.h>

namespace msdl {

class size {
public:
    using _Self = size;

public:
    size() = default;

public:
    int w, h;

};

class point {
public:
    using _Self = point;

public:
    point() = default;
    point(int x, int y)
        : x(x)
        , y(y)
    {
    }
    point(SDL_Point p)
        : _sdl(p)
    {
    }
    inline operator SDL_Point() const {
        return _sdl;
    }
    inline _Self operator +(const size &sz) const {
        return _Self(x + sz.w, y + sz.h);
    }
    inline _Self &operator +=(const size &sz) {
        x += sz.w;
        y += sz.h;
        return *this;
    }
    inline _Self operator -(const size &sz) const {
        return _Self(x - sz.w, y - sz.h);
    }
    inline _Self &operator -=(const size &sz) {
        x -= sz.w;
        y -= sz.h;
        return *this;
    }

    inline SDL_Point to_sdl() const {
        return _sdl;
    }

public:
    union {
        SDL_Point _sdl;
        struct {
            int x, y;
        };
    };

};

class line {
public:
    using _Self = line;

public:
    line(int x1, int y1, int x2, int y2)
        : x1(x1)
        , y1(y1)
        , x2(x2)
        , y2(y2)
    {
    }
    line(point p1, point p2)
        : x1(p1.x)
        , y1(p1.y)
        , x2(p2.x)
        , y2(p2.y)
    {
    }

public:
    int x1, y1, x2, y2;

};

class rect {
public:
    using _Self = rect;

public:
    rect() = default;
    rect(int x, int y, int w, int h)
        : x(x)
        , y(y)
        , w(w)
        , h(h)
    {
    }
    rect(const SDL_Rect& r)
        : _sdl(r)
    {
    }
    inline operator SDL_Rect() const {
        return _sdl;
    }

    inline const SDL_Rect& to_sdl() const {
        return _sdl;
    }

public:
    union {
        SDL_Rect _sdl;
        struct {
            int x, y, w, h;
        };
    };

};

} // msdl

#endif // _PRIMITIVES_H_
