#ifndef _DRAWINGWINDOW_H_
#define _DRAWINGWINDOW_H_

#include "windowwrap.h"
#include "drawingarea.h"

namespace msdl {

class drawing_window
    : public window_wrap
    , public drawing_area
{
public:
    using _Self = drawing_window;

public:
    drawing_window() = delete;
    drawing_window(const _Self&) = delete; // TODO
    drawing_window(_Self&&) = default;
    drawing_window(const char* title, int w, int h);
    drawing_window(const std::string& title, int w, int h);

    ~drawing_window() override = default;

    _Self& operator =(const _Self&) = delete; // TODO
    _Self& operator =(_Self&&) = default;

    void refresh();

};

} // msdl

#endif // _DRAWINGWINDOW_H_
