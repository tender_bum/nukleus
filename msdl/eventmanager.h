#ifndef _EVENTMANAGER_H_
#define _EVENTMANAGER_H_

#include "event.h"

namespace msdl {

class event_manager {
public:
    using _Self = event_manager;

public:
    event_manager();
    event_manager(const event_manager&) = delete;
    event_manager(event_manager&&) = delete; // TODO?
    ~event_manager() = default;

    event_manager &operator =(const event_manager &) = delete;
    event_manager &operator =(event_manager &&) = delete; // TODO?

    bool is_empty() const;
    const event* get_next();

protected:
    event _iter;

};

} // msdl

#endif // _EVENTMANAGER_H_
