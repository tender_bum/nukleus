#include "drawingarea.h"

#include <util/logger/logger.h>

#include <SDL_render.h>

namespace msdl {

drawing_area::drawing_area(SDL_Renderer* renderer)
    : _renderer(renderer)
{
    if (_renderer == nullptr)
        ERROR_LOG() << SDL_GetError();
    else
        INFO_LOG() << "drawing_area created";
}

drawing_area::~drawing_area() = default;

// set_point
inline void make_set_point_log(int res) {
    if (res != 0)
        ERROR_LOG() << "SDL_RenderDrawPoint failed: " << SDL_GetError();
}
void drawing_area::set_point(int x, int y) {
    make_set_point_log(
        SDL_RenderDrawPoint(_renderer.get(), x, y)
    );
}
void drawing_area::set_point(point p) {
    make_set_point_log(
        SDL_RenderDrawPoint(_renderer.get(), p.x, p.y)
    );
}

// set_line
inline void make_set_line_log(int res) {
    if (res != 0)
        ERROR_LOG() << "SDL_RenderDrawLine failed: " << SDL_GetError();
}
void drawing_area::set_line(int x1, int y1, int x2, int y2) {
    make_set_line_log(
        SDL_RenderDrawLine(_renderer.get(), x1, y1, x2, y2)
    );
}
void drawing_area::set_line(point p1, point p2) {
    make_set_line_log(
        SDL_RenderDrawLine(_renderer.get(), p1.x, p1.y, p2.x, p2.y)
    );
}
void drawing_area::set_line(const line& l) {
    make_set_line_log(
        SDL_RenderDrawLine(_renderer.get(), l.x1, l.y1, l.x2, l.y2)
    );
}

// set_rect
inline void make_set_rect_log(int res) {
    if (res != 0)
        ERROR_LOG() << "SDL_RenderDrawRect failed: " << SDL_GetError();
}
void drawing_area::set_rect(int x, int y, int w, int h) {
    make_set_rect_log(
        SDL_RenderDrawRect(_renderer.get(), &rect(x, y, w, h).to_sdl())
    );
}
void drawing_area::set_rect(const rect& r) {
    make_set_rect_log(
        SDL_RenderDrawRect(_renderer.get(), &r.to_sdl())
    );
}

// fill_rect
inline void make_fill_rect_log(int res) {
    if (res != 0)
        ERROR_LOG() << "SDL_RenderFillRect failed: " << SDL_GetError();
}
void drawing_area::fill_rect(int x, int y, int w, int h) {
    make_fill_rect_log(
        SDL_RenderFillRect(_renderer.get(), &rect(x, y, w, h).to_sdl())
    );
}
void drawing_area::fill_rect(const rect& r) {
    make_fill_rect_log(
        SDL_RenderFillRect(_renderer.get(), &r.to_sdl())
    );
}

inline void make_render_clear_log(int res) {
    if (res != 0)
        ERROR_LOG() << "SDL_RenderClear failed: " << SDL_GetError();
}
void drawing_area::clear(color col) {
    apply_color(col);
    make_render_clear_log(
        SDL_RenderClear(_renderer.get())
    );
}

int drawing_area::width() const {
    int res;
    SDL_GetRendererOutputSize(_renderer.get(), &res, nullptr);
    return res;
}

int drawing_area::height() const {
    int res;
    SDL_GetRendererOutputSize(_renderer.get(), nullptr, &res);
    return res;
}

inline void make_apply_color_log(int res) {
    if (res != 0)
        ERROR_LOG() << "SDL_SetRenderDrawColor failed: " << SDL_GetError();
}
void drawing_area::apply_color(color col) {
    make_apply_color_log(
        SDL_SetRenderDrawColor(
            _renderer.get(),
            col.r,
            col.g,
            col.b,
            col.a
        )
    );
}

} // msdl
