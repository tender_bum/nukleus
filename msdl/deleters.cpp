#include "deleters.h"

#include <util/logger/logger.h>

#include <SDL_render.h>
#include <SDL_surface.h>

namespace msdl::magic {

void sdl_deleter::operator()(SDL_Surface* p) {
    if (p != nullptr) {
        SDL_FreeSurface(p);
        LOG() << "SDL_Surface deleted";
    }
}
void sdl_deleter::operator()(SDL_Window* p) {
    if (p != nullptr) {
        SDL_DestroyWindow(p);
        LOG() << "SDL_Window deleted";
    }
}
void sdl_deleter::operator()(SDL_Renderer* p) {
    if (p != nullptr) {
        SDL_DestroyRenderer(p);
        LOG() << "SDL_Renderer deleted";
    }
}

} // msdl::magic
