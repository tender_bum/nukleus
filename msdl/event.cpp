#include "event.h"

#include <memory>

namespace msdl {

event::event(const SDL_Event &e)
    : _data(e)
{
}

event::event(SDL_Event &&e)
    : _data(std::move(e))
{
}

event::~event() {
    auto type = get_type();
    if (type == event_type::DROP_FILE || type == event_type::DROP_TEXT)
        delete []_data.drop.file;
}

event_type event::get_type() const {
    switch (_data.type) {
    case SDL_QUIT:              return event_type::QUIT;
    case SDL_WINDOWEVENT:       return event_type::WINDOW_EVENT;
    case SDL_SYSWMEVENT:        return event_type::SYSWM_EVENT;

    case SDL_KEYDOWN:           return event_type::KEY_DOWN;
    case SDL_KEYUP:             return event_type::KEY_UP;
    case SDL_TEXTEDITING:       return event_type::TEXT_EDITING;
    case SDL_TEXTINPUT:         return event_type::TEXT_INPUT;
    case SDL_KEYMAPCHANGED:     return event_type::KEYMAP_CHANGED;

    case SDL_MOUSEMOTION:       return event_type::MOUSE_MOTION;
    case SDL_MOUSEBUTTONDOWN:   return event_type::MOUSE_BTN_DOWN;
    case SDL_MOUSEBUTTONUP:     return event_type::MOUSE_BTN_UP;
    case SDL_MOUSEWHEEL:        return event_type::MOUSE_WHEEL;

    case SDL_JOYAXISMOTION:     return event_type::JOY_AXIS_MOTION;
    case SDL_JOYBALLMOTION:     return event_type::JOY_BALL_MOTION;
    case SDL_JOYHATMOTION:      return event_type::JOY_HAT_MOTION;
    case SDL_JOYBUTTONDOWN:     return event_type::JOY_BTN_DOWN;
    case SDL_JOYBUTTONUP:       return event_type::JOY_BTN_UP;
    case SDL_JOYDEVICEADDED:    return event_type::JOY_DVC_ADDED;
    case SDL_JOYDEVICEREMOVED:  return event_type::JOY_DVC_REMOVED;

    case SDL_CONTROLLERAXISMOTION:      return event_type::CTL_AXIS_MOTION;
    case SDL_CONTROLLERBUTTONDOWN:      return event_type::CTL_BTN_DOWN;
    case SDL_CONTROLLERBUTTONUP:        return event_type::CTL_BTN_UP;
    case SDL_CONTROLLERDEVICEADDED:     return event_type::CTL_DVC_ADDED;
    case SDL_CONTROLLERDEVICEREMOVED:   return event_type::CTL_DVC_REMOVED;
    case SDL_CONTROLLERDEVICEREMAPPED:  return event_type::CTL_DVC_REMAPPED;

    case SDL_FINGERDOWN:        return event_type::FINGER_DOWN;
    case SDL_FINGERUP:          return event_type::FINGER_UP;
    case SDL_FINGERMOTION:      return event_type::FINGER_MOTION;

    case SDL_DOLLARGESTURE:     return event_type::DOLLAR_GESTURE;
    case SDL_DOLLARRECORD:      return event_type::DOLLAR_RECORD;
    case SDL_MULTIGESTURE:      return event_type::MULTI_GESTURE;

    case SDL_CLIPBOARDUPDATE:   return event_type::CLIPBOARD_UPDATE;

    case SDL_DROPFILE:          return event_type::DROP_FILE;
    case SDL_DROPTEXT:          return event_type::DROP_TEXT;
    case SDL_DROPBEGIN:         return event_type::DROP_BEGIN;
    case SDL_DROPCOMPLETE:      return event_type::DROP_COMPLETE;

    case SDL_AUDIODEVICEADDED:      return event_type::AUDIO_DVC_ADDED;
    case SDL_AUDIODEVICEREMOVED:    return event_type::AUDIO_DVC_REMOVED;

    case SDL_RENDER_TARGETS_RESET:  return event_type::RENDER_TARGETS_RESET;
    case SDL_RENDER_DEVICE_RESET:   return event_type::RENDER_DVC_RESET;
    default: return event_type::UNDEFINED;
    }
}

} // msdl
