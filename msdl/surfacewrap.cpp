#include "surfacewrap.h"

#include <util/logger/logger.h>

#include <SDL_surface.h>

namespace msdl {

surface_wrap::surface_wrap(const char* path, file_format fmt)
    : _surface(nullptr)
{
    switch (fmt) {
    case file_format::BMP:
        _surface = std::unique_ptr<SDL_Surface, magic::sdl_deleter>(
            SDL_LoadBMP(path)
        );
        break;
    }
    if (_surface == nullptr)
        ERROR_LOG() << "surface_wrap failed to create";
    else
        INFO_LOG() << "surface_wrap created";
}
surface_wrap::surface_wrap(const std::string& path, file_format fmt)
    : surface_wrap(path.c_str(), fmt)
{
}

#if 0
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
const Uint32 rmask = 0xff000000;
const Uint32 gmask = 0x00ff0000;
const Uint32 bmask = 0x0000ff00;
const Uint32 amask = 0x000000ff;
#else
const Uint32 rmask = 0x000000ff;
const Uint32 gmask = 0x0000ff00;
const Uint32 bmask = 0x00ff0000;
const Uint32 amask = 0xff000000;
#endif
#endif


surface_wrap::surface_wrap(int w, int h)
    : _surface(SDL_CreateRGBSurface(
        0, w, h, 32, 0, 0, 0, 0
    ))
{
    // LOG << SDL_SetSurfaceBlendMode(... TODO
    SDL_SetSurfaceBlendMode(_surface.get(), SDL_BLENDMODE_NONE);
    if (_surface == nullptr)
        ERROR_LOG() << "surface_wrap failed to create";
    else
        INFO_LOG() << "surface_wrap created";
}

void surface_wrap::save(const char* path, file_format fmt) {
    switch (fmt) {
    case file_format::BMP:
        // LOG << SDL_SaveBMP(... TODO
        SDL_SaveBMP(_surface.get(), path);
        break;
    }
}
void surface_wrap::save(const std::string& path, file_format fmt) {
    save(path.c_str(), fmt);
}

} // msdl
