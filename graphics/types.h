#ifndef _TYPES_H_
#define _TYPES_H_

namespace gpx {

enum class axis {
    X,
    Y,
    Z
};

} // gpx

#endif // _TYPES_H_
