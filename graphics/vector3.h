#ifndef _VECTOR3_H_
#define _VECTOR3_H_

#include "vector3w.h"

#include <cmath>

namespace gpx {

template <class T>
class vector3 {
public:
    using _Self = vector3;

public:
    static constexpr int DIM = 3;

public:
    vector3() = default;
    template <class U, class = std::enable_if_t<std::is_convertible_v<U, T>>>
    vector3(const vector3<U>& p)
        : x(static_cast<T>(p.x))
        , y(static_cast<T>(p.y))
        , z(static_cast<T>(p.z))
    {
    }
    vector3(T x, T y, T z)
        : x(x)
        , y(y)
        , z(z)
    {
    }
    template <class U, class = std::enable_if_t<std::is_convertible_v<U, T>>>
    vector3(const vector3w<U>& p)
        : x(p.x / p.w)
        , y(p.y / p.w)
        , z(p.z / p.w)
    {
    }
    _Self operator +(const _Self& p) const {
        return {x + p.x, y + p.y, z + p.z};
    }
    _Self& operator +=(const _Self& p) {
        x += p.x;
        y += p.y;
        z += p.z;
        return *this;
    }
    _Self operator -(const _Self& p) const {
        return {x - p.x, y - p.y, z - p.z};
    }
    _Self& operator -=(const _Self& p) {
        x -= p.x;
        y -= p.y;
        z -= p.z;
        return *this;
    }
    _Self operator *(T s) const {
        return {x * s, y * s, z * s};
    }
    _Self& operator *=(T s) {
        x *= s;
        y *= s;
        z *= s;
        return *this;
    }
    auto operator *(const _Self& p) const {
        return x * p.x + y * p.y + z * p.z;
    }
    _Self operator ^(const _Self& p) const {
        return {y * p.z - z * p.y, z * p.x - x * p.z, x * p.y - y * p.x};
    }
    bool operator ==(const _Self& p) const {
        return x == p.x && y == p.y && z == p.z;
    }
    bool operator !=(const _Self& p) const {
        return !operator==(p);
    }
    auto length() const {
        return sqrt(x * x + y * y + z * z);
    }
    auto length2() const {
        return x * x + y * y + z * z;
    }
    _Self normalized() const {
        return *this * (1.0 / length());
    }
    _Self& normalize() {
        return *this *= (1.0 / length());
    }

public:
    union {
        T _raw[DIM];
        struct {
            T x, y, z;
        };
    };

};

using vector3f = vector3<double>;
using vector3i = vector3<int>;

} // gpx

#endif // _VECTOR3_H_
