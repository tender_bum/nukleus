#include <util/logger/logger.h>
#include <graphics/graphicswindow.h>
#include <graphics/model.h>
#include <msdl/eventmanager.h>

#include <SDL.h>

#include <fstream>
#include <cmath>

constexpr int WIDTH = 500;
constexpr int HEIGHT = 500;
constexpr double SCALE_COEF = WIDTH / 2.0;
const double PI = acos(-1);

void test_matrix() {
    auto unit = gpx::matrix3wf::unit();
    gpx::matrix3wf rnd1 = std::array<std::array<double, 4>, 4>{{
        {0, 0, 0, 1},
        {0, 0, 1, 0},
        {0, 1, 0, 0},
        {1, 0, 0, 0}
    }};
    gpx::matrix3wf rnd2 = std::array<std::array<double, 4>, 4>{{
        {0, 0, 0, 1},
        {0, 0, 2, 0},
        {0, 3, 0, 0},
        {4, 0, 0, 0}
    }};
    gpx::matrix3wf rnd3 = std::array<std::array<double, 4>, 4>{{
        {1, 2, 3, 4},
        {2, 3, 4, 4},
        {3, 4, 4, 4},
        {4, 4, 4, 4}
    }};
    LOG() << "Unit determinant: " << unit.determinant();
    LOG() << "Random1 determinant: " << rnd1.determinant();
    LOG() << "Random2 determinant: " << rnd2.determinant();
    LOG() << "Random3 determinant: " << rnd3.determinant();

}

void draw_model(const gpx::model& model, const gpx::vector3f& light_dir, const gpx::vector3f& view_dir, gpx::graphics_window& win, const gpx::matrix3wf& conv_mat, bool mode) {
    (void)view_dir;
    for (const auto& face : model.faces()) {
        const auto& a = model.vert(face[0]);
        const auto& b = model.vert(face[1]);
        const auto& c = model.vert(face[2]);
        if (mode) {
            auto a3 = gpx::vector3f(gpx::vector3wf(a.x, a.y, a.z) * conv_mat);
            auto b3 = gpx::vector3f(gpx::vector3wf(b.x, b.y, b.z) * conv_mat);
            auto c3 = gpx::vector3f(gpx::vector3wf(c.x, c.y, c.z) * conv_mat);
            win.set_line(a3.x, a3.y, b3.x, b3.y, msdl::WHITE);
            win.set_line(b3.x, b3.y, c3.x, c3.y, msdl::WHITE);
            win.set_line(c3.x, c3.y, a3.x, a3.y, msdl::WHITE);
        } else {
            auto normal = ((c - a) ^ (b - a)).normalized();
            double intensity = light_dir.normalized() * normal;
            if (intensity <= 0)
                intensity = 0;
            win.fill_triangle(
                gpx::vector3wf(a.x, a.y, a.z) * conv_mat,
                gpx::vector3wf(b.x, b.y, b.z) * conv_mat,
                gpx::vector3wf(c.x, c.y, c.z) * conv_mat,
                msdl::color(
                    255 * intensity,
                    255 * intensity,
                    255 * intensity,
                    255)
            );
        }
    }
}

int run(int argc, char* argv[]) {
    if (argc < 2) {
        ERROR_LOG() << "Model file wasn't specified";
        return 1;
    }
    gpx::graphics_window win("Test", WIDTH, HEIGHT);
    msdl::event_manager eman;

    gpx::vector3wf light_dir(0, 0, -1);
    gpx::vector3wf view_dir(0, 0, -1);

    double matrix_angle = 0;
    double perspect_delta = 4;

    auto rotation_mat = gpx::matrix3wf::rotate(gpx::axis::Y, 0);
    auto perspect_mat = gpx::matrix3wf::perspect(gpx::axis::Z, perspect_delta);
    auto standart_mat = (
        gpx::matrix3wf::stretch(SCALE_COEF, SCALE_COEF, SCALE_COEF) *
        gpx::matrix3wf::reflect(gpx::axis::Y) *
        gpx::matrix3wf::moveon(WIDTH / 2, HEIGHT / 2, 0)
    );
    auto conv_mat = rotation_mat * perspect_mat * standart_mat;

    std::ifstream fin(argv[1]);
    gpx::model model(fin);
    
    LOG() << "Faces count: " << model.faces().size();
    LOG() << "Verts count: " << model.verts().size();

    bool quit = false;
    bool mode = false;
    while (!quit) {
        while (!eman.is_empty()) {
            auto e = eman.get_next();
            if (e->get_type() == msdl::event_type::QUIT)
                quit = true;
            if (e->get_type() == msdl::event_type::KEY_DOWN) {
                switch (e->get_data().key.keysym.scancode) {
                case SDL_SCANCODE_A:
                    LOG() << "Rotated Y";
                    matrix_angle += 0.2;
                    rotation_mat = gpx::matrix3wf::rotate(gpx::axis::Y, matrix_angle);
                    conv_mat = rotation_mat * perspect_mat * standart_mat;
                    break;
                case SDL_SCANCODE_D:
                    LOG() << "Rotated Y";
                    matrix_angle -= 0.2;
                    rotation_mat = gpx::matrix3wf::rotate(gpx::axis::Y, matrix_angle);
                    conv_mat = rotation_mat * perspect_mat * standart_mat;
                    break;
                case SDL_SCANCODE_W:
                    LOG() << "Closer";
                    perspect_delta -= 0.2;
                    perspect_mat = gpx::matrix3wf::perspect(gpx::axis::Z, perspect_delta);
                    conv_mat = rotation_mat * perspect_mat * standart_mat;
                    break;
                case SDL_SCANCODE_S:
                    LOG() << "Further";
                    perspect_delta += 0.2;
                    perspect_mat = gpx::matrix3wf::perspect(gpx::axis::Z, perspect_delta);
                    conv_mat = rotation_mat * perspect_mat * standart_mat;
                    break;
                case SDL_SCANCODE_LEFT:
                    LOG() << "Rotating light";
                    light_dir *= gpx::matrix3wf::rotate(gpx::axis::Y, 0.2);
                    break;
                case SDL_SCANCODE_RIGHT:
                    LOG() << "Rotating light";
                    light_dir *= gpx::matrix3wf::rotate(gpx::axis::Y, -0.2);
                    break;
                case SDL_SCANCODE_SPACE:
                    LOG() << "Switching mode";
                    mode ^= 1;
                default:
                    break;
                }
            }
        }
        win.clear();
        draw_model(model, light_dir, view_dir * rotation_mat, win, conv_mat, mode);
        win.refresh();
    }


    return 0;
}

int main(int argc, char* argv[]) {
    test_matrix();
    SDL_Init(SDL_INIT_EVERYTHING);
    auto rc = run(argc, argv);
    SDL_Quit();

    return rc;
}
