#include "graphicswindow.h"

#include <limits>

namespace gpx {

graphics_window::graphics_window(const char* title, int w, int h)
    : drawing_window(title, w, h)
    , z_buffer(w * h, std::numeric_limits<int>::min())
{
}

graphics_window::graphics_window(const std::string& title, int w, int h)
    : drawing_window(title, w, h)
    , z_buffer(w * h, std::numeric_limits<int>::min())
{
}

void graphics_window::clear(msdl::color col) {
    drawing_window::clear(col);
    for (int i = 0; i < width() * height(); ++i)
        z_buffer[i] = std::numeric_limits<int>::min();
}

void graphics_window::fill_triangle(const vector3i& a3, const vector3i& b3, const vector3i& c3, msdl::color col) {
    auto normal = ((b3 - a3) ^ (c3 - a3));
    auto a_coef = normal.x;
    auto b_coef = normal.y;
    auto c_coef = normal.z;
    auto d_coef = normal * a3;
    if (c_coef == 0)
        return;
    apply_color(col);

    auto a = vector2i(a3.x, a3.y);
    auto b = vector2i(b3.x, b3.y);
    auto c = vector2i(c3.x, c3.y);
    if (((b - a) ^ (c - a)) < 0)
        std::swap(b, c);

    auto ag = b - a;
    auto bg = c - b;
    auto cg = a - c;

    auto w = width();
    auto h = height();
    
    for (int x = std::max(0, std::min(a.x, std::min(b.x, c.x))); x <= std::min(w - 1, std::max(a.x, std::max(b.x, c.x))); ++x) {
        for (int y = std::max(0, std::min(a.y, std::min(b.y, c.y))); y <= std::min(h - 1, std::max(a.y, std::max(b.y, c.y))); ++y) {
            vector2i cur(x, y);
            if ((ag ^ (cur - a)) >= 0 && (bg ^ (cur - b)) >= 0 && (cg ^ (cur - c)) >= 0) {
                int z = (d_coef - a_coef * x - b_coef * y) / c_coef;
                if (z_buffer[x + w * y] < z) {
                    z_buffer[x + w * y] = z;
                    set_point(cur);
                }
            }
        }
    }
}

} // gpx
