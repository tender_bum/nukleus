#ifndef _GRAPHICSWINDOW_H_
#define _GRAPHICSWINDOW_H_

#include "vector3.h"
#include "vector2.h"

#include <msdl/drawingwindow.h>

#include <vector>

namespace gpx {

class graphics_window
    : public msdl::drawing_window
{
public:
    using _Self = graphics_window;

public:
    graphics_window() = delete;
    graphics_window(const _Self&) = delete; // TODO
    graphics_window(_Self&&) = default;
    graphics_window(const char* title, int w, int h);
    graphics_window(const std::string& title, int w, int h);

    ~graphics_window() override = default;

    _Self& operator =(const _Self&) = delete; // TODO
    _Self& operator =(_Self&&) = default;

    void fill_triangle(const vector2i& a, const vector2i& b, const vector2i& c, msdl::color col);
    void fill_triangle(const vector3i& a, const vector3i& b, const vector3i& c, msdl::color col);

    void clear(msdl::color col = msdl::BLACK) override;

protected:
    std::vector<int> z_buffer;

};

} // gpx

#endif // _GRAPHICSWINDOW_H_
