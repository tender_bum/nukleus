#ifndef _VECTOR2_H_
#define _VECTOR2_H_

#include <msdl/primitives.h>

#include <cmath>

namespace gpx {

template <class T>
class vector2 {
public:
    using _Self = vector2;

public:
    static constexpr int DIM = 2;

public:
    vector2() = default;
    template <class U, class = std::enable_if_t<std::is_convertible_v<U, T>>>
    vector2(const vector2<U>& p)
        : x(static_cast<T>(p.x))
        , y(static_cast<T>(p.y))
    {
    }
    vector2(T x, T y)
        : x(x)
        , y(y)
    {
    }
    _Self operator +(const _Self& p) const {
        return {x + p.x, y + p.y};
    }
    _Self& operator +=(const _Self& p) {
        x += p.x;
        y += p.y;
        return *this;
    }
    _Self operator -(const _Self& p) const {
        return {x - p.x, y - p.y};
    }
    _Self& operator -=(const _Self& p) {
        x -= p.x;
        y -= p.y;
        return *this;
    }
    _Self operator *(T s) const {
        return {x * s, y * s};
    }
    _Self& operator *=(T s) {
        x *= s;
        y *= s;
        return *this;
    }
    auto operator *(const _Self& p) const {
        return x * p.x + y * p.y;
    }
    auto operator ^(const _Self& p) const {
        return x * p.y - y * p.x;
    }
    bool operator ==(const _Self& p) const {
        return x == p.x && y == p.y;
    }
    bool operator !=(const _Self& p) const {
        return !operator==(p);
    }
    operator msdl::point() const {
        return {x, y};
    }
    auto length() const {
        return sqrt(x * x + y * y);
    }
    auto length2() const {
        return x * x + y * y;
    }
    _Self normalized() const {
        return *this * (1.0 / length());
    }
    _Self& normalize() {
        return *this *= (1.0 / length());
    }

public:
    union {
        T _raw[DIM];
        struct {
            T x, y;
        };
    };

};

using vector2f = vector2<double>;
using vector2i = vector2<int>;

} // gpx

#endif // _VECTOR2_H_
