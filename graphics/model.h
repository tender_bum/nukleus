#ifndef _MODEL_H_
#define _MODEL_H_

#include "vector3.h"

#include <vector>

namespace gpx {

class model {
public:
    using _Self = model;

public:
    enum class file_format {
        OBJ
    };

public:
    model() = default;
    model(std::istream& fin, file_format fmt = file_format::OBJ);

    inline auto verts_count() const {
        return _verts.size();
    }
    inline auto faces_count() const {
        return _faces.size();
    }

    inline const auto& vert(std::size_t pos) const {
        return _verts[pos];
    }
    inline const auto& face(std::size_t pos) const {
        return _faces[pos];
    }

    inline const auto& verts() const {
        return _verts;
    }
    inline const auto& faces() const {
        return _faces;
    }

    void clear();

private:
    std::vector<vector3f> _verts;
    std::vector<std::vector<std::size_t>> _faces;

};

} // gpx

#endif // _MODEL_H_
