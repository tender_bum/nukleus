#ifndef _MATRIX3W_H_
#define _MATRIX3W_H_

#include "types.h"

#include <array>
#include <cmath>
#include <algorithm>

namespace gpx {

template <class T>
class matrix3w {
public:
    using _Self = matrix3w;

public:
    static constexpr int DIM = 4;

public:
    matrix3w() = default;
    matrix3w(const std::array<std::array<T, DIM>, DIM>& val)
        : _raw(val)
    {
    }
    std::array<T, DIM>& operator[](std::size_t pos) {
        return _raw[pos];
    }
    const std::array<T, DIM>& operator[](std::size_t pos) const {
        return _raw[pos];
    }
    _Self operator *(const _Self& mat) const {
        _Self tmp;
        for (std::size_t i = 0; i < DIM; ++i) {
            for (std::size_t j = 0; j < DIM; ++j) {
                tmp[i][j] = 0;
                for (std::size_t k = 0; k < DIM; ++k)
                    tmp[i][j] += _raw[i][k] * mat[k][j];
            }
        }
        return tmp;
    }
    _Self& operator *=(const _Self& mat) {
        return *this = *this * mat;
    }
    T determinant() const {
        T res = 1;
        auto mat = _raw;
        const double EPS = 1e-7;
        for (int i = 0; i < DIM - 1; ++i) {
            auto nxt = std::max_element(std::begin(mat) + i, std::end(mat), [i](const auto& a, const auto& b) {
                return std::abs(a[i]) < std::abs(b[i]);
            });
            if (&mat[i] != &*nxt) {
                swap(mat[i], *nxt);
                res *= -1;
            }
            auto tmp = mat[i][i];
            if (std::abs(tmp) < EPS)
                return 0;
            res *= tmp;
            for (int j = i; j < DIM; ++j)
                mat[i][j] /= tmp;
            for (int j = i + 1; j < DIM; ++j) {
                auto cur = mat[j][i];
                for (int k = i; k < DIM; ++k)
                    mat[j][k] -= cur * mat[i][k];
            }
        }
        return res;
    }
    static _Self rotate(axis ax, double c, double s) {
        switch (ax) {
        case axis::X:
            return std::array<std::array<T, DIM>, DIM>{{
                {1,  0, 0, 0},
                {0,  c, s, 0},
                {0, -s, c, 0},
                {0,  0, 0, 1}
            }};
        case axis::Y:
            return std::array<std::array<T, DIM>, DIM>{{
                {c, 0, -s, 0},
                {0, 1,  0, 0},
                {s, 0,  c, 0},
                {0, 0,  0, 1}
            }};
        case axis::Z:
            return std::array<std::array<T, DIM>, DIM>{{
                { c, s, 0, 0},
                {-s, c, 0, 0},
                { 0, 0, 1, 0},
                { 0, 0, 0, 1}
            }};
        default:
            return unit();
        }
    }
    static _Self rotate(axis ax, double ang) {
        return rotate(ax, cos(ang), sin(ang));
    }
    static _Self stretch(T a, T b, T c) {
        return std::array<std::array<T, DIM>, DIM>{{
            {a, 0, 0, 0},
            {0, b, 0, 0},
            {0, 0, c, 0},
            {0, 0, 0, 1}
        }};
    }
    static _Self reflect(axis ax) {
        switch (ax) {
        case axis::X:
            return std::array<std::array<T, DIM>, DIM>{{
                {-1, 0, 0, 0},
                { 0, 1, 0, 0},
                { 0, 0, 1, 0},
                { 0, 0, 0, 1}
            }};
        case axis::Y:
            return std::array<std::array<T, DIM>, DIM>{{
                {1,  0, 0, 0},
                {0, -1, 0, 0},
                {0,  0, 1, 0},
                {0,  0, 0, 1}
            }};
        case axis::Z:
            return std::array<std::array<T, DIM>, DIM>{{
                {1, 0,  0, 0},
                {0, 1,  0, 0},
                {0, 0, -1, 0},
                {0, 0,  0, 1}
            }};
        default:
            return unit();
        }
    }
    static _Self moveon(T x, T y, T z) {
        return std::array<std::array<T, DIM>, DIM>{{
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 1, 0},
            {x, y, z, 1}
        }};
    }
    static _Self project(axis ax) {
        switch (ax) {
        case axis::X:
            return std::array<std::array<T, DIM>, DIM>{{
                {0, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
            }};
        case axis::Y:
            return std::array<std::array<T, DIM>, DIM>{{
                {1, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
            }};
        case axis::Z:
            return std::array<std::array<T, DIM>, DIM>{{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 1}
            }};
        default:
            return unit();
        }
    }
    static _Self perspect(axis ax, double c) {
        switch (ax) {
        case axis::X:
            return std::array<std::array<T, DIM>, DIM>{{
                {1, 0, 0, -1.0 / c},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
            }};
        case axis::Y:
            return std::array<std::array<T, DIM>, DIM>{{
                {1, 0, 0, 0},
                {0, 1, 0, -1.0 / c},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
            }};
        case axis::Z:
            return std::array<std::array<T, DIM>, DIM>{{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, -1.0 / c},
                {0, 0, 0, 1}
            }};
        default:
            return unit();
        }
    }
    static _Self unit() {
        return std::array<std::array<T, DIM>, DIM>{{
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 1}
        }};
    };

public:
    std::array<std::array<T, DIM>, DIM> _raw;

};

using matrix3wf = matrix3w<double>;
using matrix3wi = matrix3w<int>;

} // gpx

#endif // _MATRIX3W_H_
