#include "model.h"

#include <istream>
#include <sstream>

namespace gpx {

model::model(std::istream& fin, file_format fmt) {
    switch (fmt) {
    case file_format::OBJ:
        for (std::string line; std::getline(fin, line); ) {
            std::istringstream iline(line);
            iline >> line;
            if (line == "v") {
                double x, y, z;
                iline >> x >> y >> z;
                _verts.emplace_back(x, y, z);
            } else if (line == "f") {
                std::vector<std::size_t> fc;
                while (iline >> line) {
                    std::size_t pos;
                    sscanf(line.c_str(), "%lu %*d %*d", &pos);
                    fc.push_back(pos - 1);
                }
                _faces.emplace_back(std::move(fc));
            }
        }
        break;
    }
}

void model::clear() {
    _verts.clear();
    _faces.clear();
}

} // gpx
