#ifndef _VECTOR3W_H_
#define _VECTOR3W_H_

#include "matrix3w.h"

#include <cmath>

namespace gpx {

template <class T>
class vector3w {
public:
    using _Self = vector3w;

public:
    static constexpr int DIM = 4;

public:
    vector3w() = default;
    template <class U, class = std::enable_if_t<std::is_convertible_v<U, T>>>
    vector3w(const vector3w<U>& p)
        : x(static_cast<T>(p.x))
        , y(static_cast<T>(p.y))
        , z(static_cast<T>(p.z))
        , w(static_cast<T>(p.w))
    {
    }
    vector3w(T x, T y, T z)
        : x(x)
        , y(y)
        , z(z)
        , w(static_cast<T>(1))
    {
    }
    vector3w(T x, T y, T z, T w)
        : x(x)
        , y(y)
        , z(z)
        , w(w)
    {
    }
    auto length() const {
        return sqrt(x * x + y * y + z * z) / w;
    }
    _Self operator *(const matrix3w<T>& mat) const {
        _Self tmp;
        for (std::size_t i = 0; i < DIM; ++i) {
            tmp._raw[i] = 0;
            for (std::size_t j = 0; j < DIM; ++j)
                tmp._raw[i] += _raw[j] * mat[j][i];
        }
        return tmp;
    }
    _Self& operator *=(const matrix3w<T>& mat) {
        return *this = *this * mat;
    }

public:
    union {
        T _raw[DIM];
        struct {
            T x, y, z, w;
        };
    };

};

using vector3wf = vector3w<double>;
using vector3wi = vector3w<int>;

} // gpx

#endif // _VECTOR3W_H_
